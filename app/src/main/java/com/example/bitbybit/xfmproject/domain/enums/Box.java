package com.example.bitbybit.xfmproject.domain.enums;

/**
 * Created by filipelobo on 26/05/2017.
 */

public enum Box {
    Superacao("Superação");

    public String box;

    Box(String box) {
        this.box = box;
    }

    public String getNome() {
        return box;
    }

    @Override
    public String toString() {
        return box;
    }
}
