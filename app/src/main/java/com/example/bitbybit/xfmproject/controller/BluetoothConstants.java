package com.example.bitbybit.xfmproject.controller;

/**
 * Created by filipelobo on 01/07/2017.
 */

public interface BluetoothConstants {

    public static final int MESSAGE_STATE_CHANGE = 1;
    public static final int MESSAGE_READ = 2;
    public static final int MESSAGE_WRITE = 3;
    public static final int MESSAGE_DEVICE_NAME = 4;
    public static final int MESSAGE_TOAST = 5;

    public static final int TYPE_START_BATTLE = 10;
    public static final int TYPE_ACCEPT_BATTLE = 20;
    public static final int TYPE_CHRONOMETER_START = 30;
    public static final int TYPE_CHRONOMETER_PAUSE = 40;
    public static final int TYPE_FINISH_BATTLE = 50;

    public static final String DEVICE_NAME = "device_name";
    public static final String TOAST = "toast";

}
