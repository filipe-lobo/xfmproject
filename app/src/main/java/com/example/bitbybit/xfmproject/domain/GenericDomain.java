package com.example.bitbybit.xfmproject.domain;

/**
 * Created by filipelobo on 22/05/2017.
 */

abstract class GenericDomain {
    int id;

    public GenericDomain(){
        this.id = -1;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

}

