package com.example.bitbybit.xfmproject;

import android.content.ContentValues;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.PointF;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.dlazaro66.qrcodereaderview.QRCodeReaderView;
import com.example.bitbybit.xfmproject.dao.contract.TreinoContract;
import com.example.bitbybit.xfmproject.dao.contract.WebtokenContract;
import com.example.bitbybit.xfmproject.dao.helper.CrossFDbHelper;
import com.example.bitbybit.xfmproject.domain.Treino;
import com.example.bitbybit.xfmproject.webservice.XFManagerWSClient;
import com.google.gson.Gson;
import com.loopj.android.http.TextHttpResponseHandler;

import cz.msebera.android.httpclient.Header;

public class QRCodeActivity extends AppCompatActivity implements QRCodeReaderView.OnQRCodeReadListener {

    private static final String EXTRA_MESSAGE_BOX = "box";
    private QRCodeReaderView mQrCodeReaderView;
    private PointsOverlayView pointsOverlayView;
    private FloatingActionButton mFloatingActionButton;
    private CrossFDbHelper mCrossFDbHelper;
    private String mBox;
    private boolean isFlashOn = false;

    @Override
    protected void onStart() {
        super.onStart();
        Intent intent = getIntent();
        mBox = intent.getStringExtra(EXTRA_MESSAGE_BOX);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_qrcode);

        mCrossFDbHelper = new CrossFDbHelper(this);

        mQrCodeReaderView = (QRCodeReaderView) findViewById(R.id.qrdecoderview);
        mQrCodeReaderView.setOnQRCodeReadListener(this);
        mQrCodeReaderView.setQRDecodingEnabled(true);
        mQrCodeReaderView.setAutofocusInterval(2000L);
        mQrCodeReaderView.setTorchEnabled(true);
        mQrCodeReaderView.setFrontCamera();
        mQrCodeReaderView.setBackCamera();

        pointsOverlayView = (PointsOverlayView) findViewById(R.id.points_overlay_view);

        mFloatingActionButton = (FloatingActionButton) findViewById(R.id.fab_flash_bttn);
        mFloatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(isFlashOn) {
                    mFloatingActionButton.setImageResource(R.drawable.ic_flash_off_black_24dp);
                    mQrCodeReaderView.setTorchEnabled(false);
                    isFlashOn = false;
                } else {
                    mFloatingActionButton.setImageResource(R.drawable.ic_flash_on_black_24dp);
                    mQrCodeReaderView.setTorchEnabled(true);
                    isFlashOn = true;
                }
            }
        });
    }

    @Override
    public void onQRCodeRead(String text, PointF[] points) {
        pointsOverlayView.setPoints(points);
        String suffix = "treino/treinododia/" + mBox;
        String url = getString(R.string.url_webservice_base) + suffix;
        Log.d("USER URL", url);
        Log.d("COMPARE", String.valueOf(text.equals(url)));
        if(text.equals(url)) {
            XFManagerWSClient.get(suffix, null, new TextHttpResponseHandler() {
                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {

                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, String responseString) {
                    if(!responseString.isEmpty()) {
                        Gson gson = new Gson();
                        Treino treino = null;
                        try{
                            treino = gson.fromJson(responseString, Treino.class);
                            saveTreinoOnDb(treino);
                            onBackPressed();
                        } catch(Exception e) {

                        }
                    }
                }
            });
        } else {
            Log.d("QRCode", text);
        }
    }

    private long saveTreinoOnDb(Treino treino) {
        SQLiteDatabase db = mCrossFDbHelper.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(TreinoContract.TreinoEntry.COLUMN_BOX, treino.getBox().getNome());
        values.put(TreinoContract.TreinoEntry.COLUMN_USUARIO, treino.getUsuario() != null ? treino.getUsuario().getUsuario() : "");
        values.put(TreinoContract.TreinoEntry.COLUMN_DATA, treino.getData());
        values.put(TreinoContract.TreinoEntry.COLUMN_WOD_PRINCIPAL, treino.getWodPrincipal());
        values.put(TreinoContract.TreinoEntry.COLUMN_WOD_DESAFIO, treino.getWodDesafio());
        values.put(TreinoContract.TreinoEntry.COLUMN_WOD_EXTRA, treino.getWodExtra());
        values.put(TreinoContract.TreinoEntry.COLUMN_TIPO, treino.isTipo());

        long newRowId = db.insert(TreinoContract.TreinoEntry.TABLE_NAME, null, values);
        Log.d("ID", String.valueOf(newRowId));
        db.close();
        return newRowId;
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    public interface OnRightQrCodeRead {
        public void onRightQrCodeRead();
    }
}
