package com.example.bitbybit.xfmproject.domain.enums;

/**
 * Created by filipelobo on 27/06/2017.
 */

public enum Soldiers {

    JESSICA("Jessica Squat", "150 wall ball - Mulher 6 KG e Homem 8 KG", 10),
    ARNOLD("Arnold LPO", "21-15-9 Clean and Burpees", 15),
    BRUCE("Bruce LPO", "21-15-9 Snatchs", 10),
    CAMILA("Camila Dumbell", "200 Dumbells", 10);

    public String nome;
    public String detalhes;
    public int minutos;

    Soldiers(String nome, String detalhes, int minutos) {
        this.nome = nome;
        this.detalhes = detalhes;
        this.minutos = minutos;
    }

    public String getNome() {
        return nome;
    }

    public String getDetalhes() {
        return detalhes;
    }

    public int getMinutos() {
        return minutos;
    }
}
