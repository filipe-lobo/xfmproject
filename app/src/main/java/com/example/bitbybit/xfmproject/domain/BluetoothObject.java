package com.example.bitbybit.xfmproject.domain;

import com.example.bitbybit.xfmproject.controller.BluetoothService;

/**
 * Created by filipelobo on 02/07/2017.
 */

public class BluetoothObject {

    private int type;
    private int idPessoa;
    private String nomePessoa;
    private Exercicio exercicio;

    public BluetoothObject(int type, Pessoa pessoa, Exercicio exercicio) {
        this.type = type;
        this.idPessoa = pessoa.getUsuario().getIdPessoa();
        this.nomePessoa = pessoa.getNome();
        this.exercicio = exercicio;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public Exercicio getExercicio() {
        return exercicio;
    }

    public void setExercicio(Exercicio exercicio) {
        this.exercicio = exercicio;
    }

    public String getNomePessoa() {
        return nomePessoa;
    }

    public void setNomePessoa(String nomePessoa) {
        this.nomePessoa = nomePessoa;
    }

    public int getIdPessoa() {
        return idPessoa;
    }

    public void setIdPessoa(int idPessoa) {
        this.idPessoa = idPessoa;
    }
}
