package com.example.bitbybit.xfmproject.controller;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.bitbybit.xfmproject.R;
import com.example.bitbybit.xfmproject.UpdatePRDialogFragment;
import com.example.bitbybit.xfmproject.domain.PersonalRecords;
import com.example.bitbybit.xfmproject.domain.enums.LPO;
import com.example.bitbybit.xfmproject.domain.enums.LPO;

import java.util.ArrayList;
import java.util.Collection;
import java.util.EnumMap;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * Created by filipelobo on 07/05/2017.
 */

public class PRItemAdapter extends RecyclerView.Adapter<PRItemAdapter.ProgressItemViewHolder> {

    private AdapterCallback mAdapterCallback;

    private ArrayList allExercises;
    PersonalRecords mPersonalRecords;
    private Context context;

    public PRItemAdapter(Context context, PersonalRecords personalRecords) {
        this.context = context;

        try {
            this.mAdapterCallback = ((AdapterCallback) context);
        } catch (ClassCastException e) {
            throw new ClassCastException("Activity must implement AdapterCallback.");
        }

        if(personalRecords == null) {
            mPersonalRecords = new PersonalRecords();
        } else {
            mPersonalRecords = personalRecords;
        }

        if(mPersonalRecords.getConjuntoPRs() == null) {
            mPersonalRecords.setConjuntoPRs(new HashMap<String, Integer>());
        }

        this.allExercises = new ArrayList(EnumSet.allOf(LPO.class));
    }

    @Override
    public ProgressItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        context = parent.getContext();
        int layoutIdForListItem = R.layout.list_item_pr;
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        boolean shouldAttachToParentImmediately = false;

        View view = layoutInflater.inflate(layoutIdForListItem, parent, shouldAttachToParentImmediately);
        ProgressItemViewHolder progressItemViewHolder = new ProgressItemViewHolder(view);

        return progressItemViewHolder;
    }

    @Override
    public void onBindViewHolder(ProgressItemViewHolder holder, final int position) {
        final LPO exercicio = (LPO) allExercises.get(position);
        holder.listItemExerciseLabel.setText(exercicio.toString());
        if(mPersonalRecords.getConjuntoPRs().get(exercicio.getNome()) != null) {
            holder.listItemWeightLabel.setText(mPersonalRecords.getConjuntoPRs().get(exercicio.getNome()).toString());
        } else {
            holder.listItemWeightLabel.setText("0");
        }
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    mAdapterCallback.onClickMethodCallback(exercicio, mPersonalRecords);
                } catch (ClassCastException exception) {
                    exception.printStackTrace();
                }
            }
        });
    }

    public void updateList(ArrayList<LPO> list){
        allExercises = list;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return allExercises.size();
    }

    class ProgressItemViewHolder extends RecyclerView.ViewHolder {

        TextView listItemExerciseLabel, listItemWeightLabel;

        public ProgressItemViewHolder(View itemView) {
            super(itemView);

            listItemExerciseLabel = (TextView) itemView.findViewById(R.id.tv_progress_exercise_label);
            listItemWeightLabel = (TextView) itemView.findViewById(R.id.tv_progress_weight_label);

        }
    }

    public static interface AdapterCallback {
        void onClickMethodCallback(LPO exercicio, PersonalRecords prs);
    }

}
