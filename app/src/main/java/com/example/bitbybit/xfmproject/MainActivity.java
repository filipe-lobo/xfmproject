package com.example.bitbybit.xfmproject;

import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.NavigationView;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.util.SparseArray;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.example.bitbybit.xfmproject.controller.BattleOptionsAdapter;
import com.example.bitbybit.xfmproject.controller.CustomFragmentViewPager;
import com.example.bitbybit.xfmproject.controller.PRItemAdapter;
import com.example.bitbybit.xfmproject.dao.contract.PersonalRecordsContract;
import com.example.bitbybit.xfmproject.dao.contract.PessoaContract;
import com.example.bitbybit.xfmproject.dao.contract.TreinoContract;
import com.example.bitbybit.xfmproject.dao.contract.UsuarioContract;
import com.example.bitbybit.xfmproject.dao.contract.WebtokenContract;
import com.example.bitbybit.xfmproject.dao.helper.CrossFDbHelper;
import com.example.bitbybit.xfmproject.domain.Exercicio;
import com.example.bitbybit.xfmproject.domain.PersonalRecords;
import com.example.bitbybit.xfmproject.domain.Pessoa;
import com.example.bitbybit.xfmproject.domain.Usuario;
import com.example.bitbybit.xfmproject.domain.enums.LPO;
import com.example.bitbybit.xfmproject.utils.CircleTransform;
import com.example.bitbybit.xfmproject.webservice.XFManagerWSClient;
import com.google.gson.Gson;
import com.loopj.android.http.TextHttpResponseHandler;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.Map;

import cz.msebera.android.httpclient.Header;

public class MainActivity extends AppCompatActivity implements PRItemAdapter.AdapterCallback,
        UpdatePRDialogFragment.UpdatePRDialogListener {

    private static final int MY_PERMISSIONS_REQUEST_CAMERA = 10;

    private NavigationView navigationView;
    private DrawerLayout drawerLayout;
    private View navHeader;
    private ImageView imgNavHeaderBg, imgProfile;
    private TextView txtName, txtBox, txtPoints;
    private Toolbar toolbar;
    private BottomNavigationView bottomNavigationView;
    private Pessoa mPessoa;
    private CrossFDbHelper mCrossFDbHelper;

    private PessoaChangeListener listener;

    private static final String urlProfileImg = "http://www.wheretotonight.com/melbourne/images/empty_profile.png";

    public static int navItemIndex = 0;
    private CustomFragmentViewPager mViewPager;
    private FragmentStatePagerAdapter mFragmentPagerAdapter;

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener;

    @Override
    protected void onStart() {
        super.onStart();
        doIfFirstBoot();
    }

    private void doIfFirstBoot() {
        mCrossFDbHelper = new CrossFDbHelper(this);

        ArrayList<Usuario> arrayList = getWebtokenFromDb();

        if(arrayList.size() < 1) {
            Intent intent = new Intent(this, LoginActivity.class);
            startActivity(intent);
            this.finish();
        } else {
            Usuario usuario = arrayList.get(0);
            if (usuario.isProfessor()) {
                Intent intent = new Intent(this, TrainerMainActivity.class);
                startActivity(intent);
                this.finish();
            } else {
                if(!checkPersonalInformationOnDb()) {
                    getUserInformationFromWeb(usuario);
                } else {
                    mPessoa = getPersonalInformationFromDb();
                    Log.d("FROMDB", "Got User from DB!!!");
                    loadNavHeader();
                    setUpFragmentNavigation();
                }
            }
        }
    }

    private ArrayList<Usuario> getWebtokenFromDb() {
        ArrayList<Usuario> array = new ArrayList<Usuario>();
        String selectQuery = "SELECT * FROM " + UsuarioContract.UsuarioEntry.TABLE_NAME;
        SQLiteDatabase db = mCrossFDbHelper.getReadableDatabase();
        try {
            Cursor cursor = db.rawQuery(selectQuery, null);
            try {
                if(cursor.moveToFirst()) {
                    do {
                        Usuario usuario = new Usuario();
                        usuario.setUsuario(cursor.getString(1));
                        usuario.setIdPessoa(cursor.getInt(2));
                        usuario.setAtivo(cursor.getInt(3) != 0 ? true : false);
                        usuario.setProfessor(cursor.getInt(4) != 0 ? true : false);
                        array.add(usuario);
                    } while(cursor.moveToNext());
                }
            } finally {
                try {
                    cursor.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } finally {
            try {
                db.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return array;
    }

    private boolean checkPersonalInformationOnDb() {
        String selectionQuery = "SELECT * FROM " + PessoaContract.PessoaEntry.TABLE_NAME;
        SQLiteDatabase db = mCrossFDbHelper.getReadableDatabase();
        try {
            Cursor cursor = db.rawQuery(selectionQuery, null);
            if(cursor.getCount() > 0) {
                cursor.close();
                db.close();
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    private Pessoa getPersonalInformationFromDb() {
        SQLiteDatabase db = mCrossFDbHelper.getReadableDatabase();
        String query = "SELECT * FROM " + PessoaContract.PessoaEntry.TABLE_NAME;
        Pessoa pessoa = new Pessoa();
        try {
            Cursor cursor = db.rawQuery(query, null);
            cursor.moveToFirst();
            pessoa.setNome(cursor.getString(1));
            pessoa.setIdade(cursor.getInt(2));
            pessoa.setAniversario(cursor.getString(3));
            pessoa.setCelular(cursor.getString(4));
            pessoa.setBox(cursor.getString(5));
            pessoa.setVencimento(cursor.getString(6));
            pessoa.setPontos(cursor.getInt(7));

            query = "SELECT * FROM " + UsuarioContract.UsuarioEntry.TABLE_NAME;
            cursor = db.rawQuery(query, null);
            cursor.moveToFirst();
            Usuario usuario = new Usuario();
            usuario.setUsuario(cursor.getString(1));
            usuario.setIdPessoa(cursor.getInt(2));
            usuario.setAtivo(cursor.getInt(3) != 0 ? true : false);
            usuario.setProfessor(cursor.getInt(4) != 0 ? true : false);

            query = "SELECT * FROM " + PersonalRecordsContract.PersonalRecordsEntry.TABLE_NAME;
            PersonalRecords prs = new PersonalRecords();
            Map<String, Integer> prsValues = new HashMap<String, Integer>();
            cursor = db.rawQuery(query, null);
            cursor.moveToFirst();
            prs.setId(cursor.getInt(0));
            prsValues.put(LPO.SNATCH.getNome(), cursor.getInt(1));
            prsValues.put(LPO.SQUATSNATCH.getNome(), cursor.getInt(2));
            prsValues.put(LPO.POWERSNATCH.getNome(), cursor.getInt(3));
            prsValues.put(LPO.SNATCHBALANCE.getNome(), cursor.getInt(4));
            prsValues.put(LPO.COMPLEXSNATCH.getNome(), cursor.getInt(5));
            prsValues.put(LPO.CLEAN.getNome(), cursor.getInt(6));
            prsValues.put(LPO.SQUATCLEAN.getNome(), cursor.getInt(7));
            prsValues.put(LPO.POWERCLEAN.getNome(), cursor.getInt(8));
            prsValues.put(LPO.COMPLEXCLEAN.getNome(), cursor.getInt(9));
            prsValues.put(LPO.CLEANANDJEARK.getNome(), cursor.getInt(10));
            prs.setConjuntoPRs(prsValues);

            pessoa.setUsuario(usuario);
            pessoa.setPrs(prs);

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            db.close();
        }
        return pessoa;
    }

    private void getUserInformationFromWeb(Usuario usuario) {
        try {

            String urlbodyString = String.format("usuario/carregarinformacoes/" + usuario.getIdPessoa());

            XFManagerWSClient.get(urlbodyString, null, new TextHttpResponseHandler() {

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    Log.d("RESPONSE STRING", responseString);
                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, String responseString) {
                    if(!(responseString.isEmpty())){
                        Log.d("RESPONSE STRING", responseString);
                        try {
                            Gson gson = new Gson();
                            mPessoa = gson.fromJson(responseString, Pessoa.class);
                            savePersonalInformationOnDb(mPessoa);
                            Log.d("FROMWEB", "Got User from Web!");
                            loadNavHeader();
                            setUpFragmentNavigation();

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } else {
                        System.out.println("User Information empty!");
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void savePersonalInformationOnDb(Pessoa pessoa) {
        SQLiteDatabase db = mCrossFDbHelper.getWritableDatabase();

        ContentValues contentValues = new ContentValues();
        contentValues.put(PessoaContract.PessoaEntry.COLUMN_NOME, pessoa.getNome());
        contentValues.put(PessoaContract.PessoaEntry.COLUMN_IDADE, pessoa.getIdade());
        contentValues.put(PessoaContract.PessoaEntry.COLUMN_ANIVERSARIO, pessoa.getAniversario());
        contentValues.put(PessoaContract.PessoaEntry.COLUMN_CELULAR, pessoa.getCelular());
        contentValues.put(PessoaContract.PessoaEntry.COLUMN_BOX, pessoa.getBox());
        contentValues.put(PessoaContract.PessoaEntry.COLUMN_VENCIMENTO, pessoa.getVencimento());
        contentValues.put(PessoaContract.PessoaEntry.COLUMN_PONTOS, pessoa.getPontos());

        long newPessoaId = db.insert(PessoaContract.PessoaEntry.TABLE_NAME, null, contentValues);

        PersonalRecords prs = pessoa.getPrs();
        contentValues = new ContentValues();
        contentValues.put(PersonalRecordsContract.PersonalRecordsEntry._ID, pessoa.getId());
        if(prs != null && prs.getConjuntoPRs() != null) {
            contentValues.put(PersonalRecordsContract.PersonalRecordsEntry.COLUMN_SNATCH, mapValueOrEmpty(prs.getConjuntoPRs(), LPO.SNATCH));
            contentValues.put(PersonalRecordsContract.PersonalRecordsEntry.COLUMN_SQUATSNATCH, mapValueOrEmpty(prs.getConjuntoPRs(), LPO.SQUATSNATCH));
            contentValues.put(PersonalRecordsContract.PersonalRecordsEntry.COLUMN_POWERSNATCH, mapValueOrEmpty(prs.getConjuntoPRs(), LPO.POWERSNATCH));
            contentValues.put(PersonalRecordsContract.PersonalRecordsEntry.COLUMN_SNATCHBALANCE, mapValueOrEmpty(prs.getConjuntoPRs(), LPO.SNATCHBALANCE));
            contentValues.put(PersonalRecordsContract.PersonalRecordsEntry.COLUMN_COMPLEXSNATCH, mapValueOrEmpty(prs.getConjuntoPRs(), LPO.COMPLEXSNATCH));
            contentValues.put(PersonalRecordsContract.PersonalRecordsEntry.COLUMN_CLEAN, mapValueOrEmpty(prs.getConjuntoPRs(), LPO.CLEAN));
            contentValues.put(PersonalRecordsContract.PersonalRecordsEntry.COLUMN_SQUATCLEAN, mapValueOrEmpty(prs.getConjuntoPRs(), LPO.SQUATCLEAN));
            contentValues.put(PersonalRecordsContract.PersonalRecordsEntry.COLUMN_POWERCLEAN, mapValueOrEmpty(prs.getConjuntoPRs(), LPO.POWERCLEAN));
            contentValues.put(PersonalRecordsContract.PersonalRecordsEntry.COLUMN_COMPLEXCLEAN, mapValueOrEmpty(prs.getConjuntoPRs(), LPO.COMPLEXCLEAN));
            contentValues.put(PersonalRecordsContract.PersonalRecordsEntry.COLUMN_CLEANANDJERK, mapValueOrEmpty(prs.getConjuntoPRs(), LPO.CLEANANDJEARK));
        } else {
            contentValues.put(PersonalRecordsContract.PersonalRecordsEntry.COLUMN_SNATCH, 0);
            contentValues.put(PersonalRecordsContract.PersonalRecordsEntry.COLUMN_SQUATSNATCH, 0);
            contentValues.put(PersonalRecordsContract.PersonalRecordsEntry.COLUMN_POWERSNATCH, 0);
            contentValues.put(PersonalRecordsContract.PersonalRecordsEntry.COLUMN_SNATCHBALANCE, 0);
            contentValues.put(PersonalRecordsContract.PersonalRecordsEntry.COLUMN_COMPLEXSNATCH, 0);
            contentValues.put(PersonalRecordsContract.PersonalRecordsEntry.COLUMN_CLEAN, 0);
            contentValues.put(PersonalRecordsContract.PersonalRecordsEntry.COLUMN_SQUATCLEAN, 0);
            contentValues.put(PersonalRecordsContract.PersonalRecordsEntry.COLUMN_POWERCLEAN, 0);
            contentValues.put(PersonalRecordsContract.PersonalRecordsEntry.COLUMN_COMPLEXCLEAN, 0);
            contentValues.put(PersonalRecordsContract.PersonalRecordsEntry.COLUMN_CLEANANDJERK, 0);
        }

        long newPrsId = db.insert(PersonalRecordsContract.PersonalRecordsEntry.TABLE_NAME, null, contentValues);

        db.close();
    }

    private int mapValueOrEmpty(Map<String, Integer> map, LPO exercicio) {
        Integer value = map.get(exercicio.getNome());
        return value == null ? 0 : value;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        navigationView = (NavigationView) findViewById(R.id.nav_view);

        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                int id = item.getItemId();

                if (id == R.id.logout) {
                    AlertDialog.Builder builder;
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        builder = new AlertDialog.Builder(MainActivity.this, R.style.Theme_AppCompat_Light_Dialog_Alert);
                    } else {
                        builder = new AlertDialog.Builder(MainActivity.this);
                    }
                    builder.setTitle("Logout")
                            .setMessage("Tem certeza que deseja fazer logout?")
                            .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    SQLiteDatabase db = mCrossFDbHelper.getWritableDatabase();
                                    db.delete(WebtokenContract.WebtokenEntry.TABLE_NAME, "1", null);
                                    db.delete(UsuarioContract.UsuarioEntry.TABLE_NAME, "1", null);
                                    db.delete(PessoaContract.PessoaEntry.TABLE_NAME, "1", null);
                                    db.delete(PersonalRecordsContract.PersonalRecordsEntry.TABLE_NAME, "1", null);
                                    db.delete(TreinoContract.TreinoEntry.TABLE_NAME, "1", null);

                                    Intent intent = new Intent(getBaseContext(), LoginActivity.class);
                                    startActivity(intent);
                                    finish();
                                }
                            })
                            .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {

                                }
                            })
                            .setIcon(android.R.drawable.ic_dialog_alert)
                            .show();
                }
                if (id == R.id.showProfile) {
                    Intent intent = new Intent(getBaseContext(), PersonalInformationActivity.class);
                    startActivity(intent);
                }

                drawerLayout.closeDrawer(GravityCompat.START);
                return true;
            }
        });

        bottomNavigationView = (BottomNavigationView) findViewById(R.id.navigation);

        setUpNavigationView();

        loadNavHeader();

        if (savedInstanceState == null) {
            navItemIndex = 0;
        }
    }

    private void loadNavHeader() {
        navHeader = navigationView.getHeaderView(0);
        txtName = (TextView) navHeader.findViewById(R.id.name);
        txtBox = (TextView) navHeader.findViewById(R.id.main_header_box);
        txtPoints = (TextView) navHeader.findViewById(R.id.main_header_points);
        // imgNavHeaderBg = (ImageView) navHeader.findViewById(R.id.img_header_bg);
        // imgProfile = (ImageView) navHeader.findViewById(R.id.img_profile);

        /**
        Glide.with(this).load(urlProfileImg)
                .crossFade()
                .thumbnail(0.5f)
                .bitmapTransform(new CircleTransform(this))
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(imgProfile);
         **/

        try {
            txtName.setText(String.format(mPessoa.getNome() + ", " + mPessoa.getIdade()));
            txtBox.setText(mPessoa.getBox());
            txtPoints.setText(String.valueOf(mPessoa.getPontos()));
        } catch (Exception e) {
            e.printStackTrace();
            txtName.setText("Username");
            txtBox.setText("Box");
            txtPoints.setText("0");
        }
    }

    private void setUpNavigationView() {
        mOnNavigationItemSelectedListener
                = new BottomNavigationView.OnNavigationItemSelectedListener() {

            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.navigation_wod:
                        mViewPager.setCurrentItem(0);
                        break;
                    case R.id.navigation_progress:
                        mViewPager.setCurrentItem(1);
                        break;
                    case R.id.navigation_battle:
                        mViewPager.setCurrentItem(2);
                        break;
                }

                if (item.isChecked()) {
                    item.setChecked(false);
                } else {
                    item.setChecked(true);
                }
                item.setChecked(true);

                return true;
            }

        };

        bottomNavigationView.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        ActionBarDrawerToggle actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.openDrawer, R.string.closeDrawer) {

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
            }
        };

        drawerLayout.setDrawerListener(actionBarDrawerToggle);

        actionBarDrawerToggle.syncState();
    }

    private void setUpFragmentNavigation() {
        mViewPager = (CustomFragmentViewPager) findViewById(R.id.content);
        mFragmentPagerAdapter = new FragmentStatePagerAdapter(this.getSupportFragmentManager()) {

            SparseArray<Fragment> registeredFragments = new SparseArray<Fragment>();

            private final static int NUM_ITEMS = 3;

            @Override
            public Fragment getItem(int position) {
                switch (position) {
                    case 0:
                        return WODFragment.newInstance(mPessoa);
                    case 1:
                        return PRFragment.newInstance(mPessoa);
                    case 2:
                        return BattleFragment.newInstance(mPessoa);
                    default:
                        return null;
                }
            }

            @Override
            public int getCount() {
                return NUM_ITEMS;
            }

            @Override
            public Object instantiateItem(ViewGroup container, int position) {
                Fragment fragment = (Fragment) super.instantiateItem(container, position);
                registeredFragments.put(position, fragment);
                return fragment;
            }

            @Override
            public void destroyItem(ViewGroup container, int position, Object object) {
                registeredFragments.remove(position);
                super.destroyItem(container, position, object);
            }

            public Fragment getRegisteredFragment(int position) {
                return registeredFragments.get(position);
            }

            @Override
            public int getItemPosition(Object object) {
                return POSITION_NONE;
            }
        };
        mViewPager.setAdapter(mFragmentPagerAdapter);
        mViewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                bottomNavigationView.getMenu().getItem(position).setChecked(true);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    @Override
    public void onBackPressed() {
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawers();
            return;
        }
        super.onBackPressed();
    }

    @Override
    protected void onDestroy() {
        mCrossFDbHelper.close();
        super.onDestroy();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_CAMERA: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    Intent intent = new Intent(this, QRCodeActivity.class);
                    startActivity(intent);

                }
                return;
            }
        }

    }

    @Override
    public void onClickMethodCallback(LPO exercicio, PersonalRecords prs) {
        DialogFragment dialog = new UpdatePRDialogFragment(exercicio, prs);
        dialog.show(getSupportFragmentManager(), "UpdatePRDialogFragment");
    }

    @Override
    public void onDialogPositiveClick(DialogFragment dialog) {
        mPessoa = getPersonalInformationFromDb();
        Log.d("NEW SNATCH", mPessoa.getPrs().getConjuntoPRs().get(LPO.SNATCH.getNome()).toString());
        mViewPager.getAdapter().notifyDataSetChanged();
    }

    public interface PessoaChangeListener {
        public void onChange(Pessoa pessoa);
    }
}
