package com.example.bitbybit.xfmproject.domain.enums;

/**
 * Created by filipelobo on 27/06/2017.
 */

public enum LPO {
    SNATCH("Snatch",true), SQUATSNATCH("Squat Snatch",true), POWERSNATCH("Power Snatch",true),
    SNATCHBALANCE("Snatch Balance",false),COMPLEXSNATCH("Complex Snatch",false), CLEAN("Clean",true),
    SQUATCLEAN("Squat Clean",true), POWERCLEAN("Power Clean",true),COMPLEXCLEAN("Complex Clean",false),
    CLEANANDJEARK("Clean and Jeark",true);

    public String nome;
    public boolean gravavel;

    LPO(String nome, boolean gravavel) {
        this.nome = nome;
        this.gravavel = gravavel;
    }

    public String getNome() {
        return nome;
    }

    public boolean isGravavel() {
        return gravavel;
    }

    @Override
    public String toString() {
        return this.nome;
    }
}