package com.example.bitbybit.xfmproject.controller;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Color;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.bitbybit.xfmproject.R;
import com.example.bitbybit.xfmproject.domain.Exercicio;

import java.util.List;

/**
 * Created by filipelobo on 02/07/2017.
 */

public class BattleOptionsAdapter extends RecyclerView.Adapter<BattleOptionsAdapter.BattleOptionsViewHolder> {

    private Context context;
    private Exercicio[] exercicios;
    private OnBattleOptionsItemClickListener listener;
    private int mCheckedPosition = -1;

    public BattleOptionsAdapter(Context context, Exercicio[] exercicios, OnBattleOptionsItemClickListener listener) {
        this.context = context;
        this.exercicios = exercicios;
        this.listener = listener;
    }

    @Override
    public BattleOptionsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();

        int layoutIdForListItem = R.layout.list_item_battle_option;
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        boolean shouldAttachToParentImmediately = false;

        View view = layoutInflater.inflate(layoutIdForListItem, parent, shouldAttachToParentImmediately);
        BattleOptionsViewHolder progressItemViewHolder = new BattleOptionsViewHolder(view);

        return progressItemViewHolder;
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onBindViewHolder(final BattleOptionsViewHolder holder, final int position) {
        final Exercicio exercicio = exercicios[position];
        holder.mNameBattleOption.setText(exercicio.getExercicio().toString());
        holder.mExerciseBattleOption.setText(exercicio.getRepeticoes());
        holder.mTimeBattleOption.setText(String.valueOf(exercicio.getMinutos()) + " minutos");
        holder.itemView.setBackgroundColor(position == mCheckedPosition ?
                context.getColor(R.color.colorScaleGray) :
                context.getColor(android.R.color.background_light));
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @TargetApi(Build.VERSION_CODES.M)
            @Override
            public void onClick(View v) {
                if (position == mCheckedPosition) {
                    holder.itemView.setBackgroundColor(context.getColor(android.R.color.background_light));
                    mCheckedPosition = -1;
                    listener.onBattleOptionsItemClick(null);
                } else {
                    mCheckedPosition = position;
                    notifyDataSetChanged();
                    listener.onBattleOptionsItemClick(exercicio);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return exercicios.length;
    }

    class BattleOptionsViewHolder extends RecyclerView.ViewHolder {

        TextView mNameBattleOption, mExerciseBattleOption, mTimeBattleOption;

        public BattleOptionsViewHolder(View itemView) {
            super(itemView);

            mNameBattleOption = (TextView) itemView.findViewById(R.id.tv_battle_option_name);
            mExerciseBattleOption = (TextView) itemView.findViewById(R.id.tv_battle_option_exercise);
            mTimeBattleOption = (TextView) itemView.findViewById(R.id.tv_battle_option_time);

        }
    }

    public interface OnBattleOptionsItemClickListener {
        void onBattleOptionsItemClick(Exercicio exercicio);
    }

}
