package com.example.bitbybit.xfmproject.domain;

/**
 * Created by filipelobo on 26/05/2017.
 */

public class Box extends GenericDomain {
    private String nome;
    private int alunosAtivos;

    public Box(com.example.bitbybit.xfmproject.domain.enums.Box box) {
        this.nome = box.getNome();
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public int getAlunosAtivos() {
        return alunosAtivos;
    }

    public void setAlunosAtivos(int alunosAtivos) {
        this.alunosAtivos = alunosAtivos;
    }
}
