package com.example.bitbybit.xfmproject.dao.contract;

import android.provider.BaseColumns;

/**
 * Created by filipelobo on 16/06/2017.
 */

public class TreinoContract {
    public static final class TreinoEntry implements BaseColumns {
        public static final String TABLE_NAME = "treino";
        public static final String COLUMN_BOX = "box";
        public static final String COLUMN_USUARIO = "usuario";
        public static final String COLUMN_DATA = "data";
        public static final String COLUMN_WOD_PRINCIPAL = "wodPrincipal";
        public static final String COLUMN_WOD_DESAFIO = "wodDesafio";
        public static final String COLUMN_WOD_EXTRA = "wodExtra";
        public static final String COLUMN_TIPO = "tipo";
    }
}
