package com.example.bitbybit.xfmproject.controller;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.bitbybit.xfmproject.R;
import com.example.bitbybit.xfmproject.domain.Treino;

/**
 * Created by filipelobo on 31/05/2017.
 */

public class WODItemAdapter extends RecyclerView.Adapter<WODItemAdapter.WODItemViewHolder> {

    private static int NUM_ITENS = 3;
    private Context context;
    private Treino mTreino;

    public WODItemAdapter(Context context, Treino treino) {
        this.mTreino = treino;
        this.context = context;
    }

    @Override
    public WODItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        context = parent.getContext();
        int layoutIdForListItem = R.layout.list_item_wod;
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        boolean shouldAttachToParentImmediately = false;

        View view = layoutInflater.inflate(layoutIdForListItem, parent, shouldAttachToParentImmediately);
        WODItemViewHolder wodItemViewHolder = new WODItemViewHolder(view);

        return wodItemViewHolder;
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void onBindViewHolder(WODItemViewHolder holder, int position) {
        switch(position) {
            case 0: {
                holder.mWodItemLabel.setText("WOD Principal");
                holder.mWodItemContent.setText(mTreino.getWodPrincipal());
                break;
            }
            case 1: {
                holder.mWodItemLabel.setText("WOD Desafio");
                holder.mWodItemContent.setText(mTreino.getWodDesafio());
                break;
            }
            case 2: {
                holder.mWodItemLabel.setText("WOD Extra");
                holder.mWodItemContent.setText(mTreino.getWodExtra());
                break;
            }
            default: {
                holder.mWodItemLabel.setText("WOD #" + (position + 1));
                holder.mWodItemContent.setText("WOD Content #" + (position + 1));
                break;
            }
        }
        holder.itemView.setElevation(20f);
    }

    @Override
    public int getItemCount() {
        return NUM_ITENS;
    }

    class WODItemViewHolder extends RecyclerView.ViewHolder {

        TextView mWodItemLabel, mWodItemContent;

        public WODItemViewHolder(View itemView) {
            super(itemView);

            mWodItemLabel = (TextView) itemView.findViewById(R.id.tv_wod_item_label);
            mWodItemContent = (TextView) itemView.findViewById(R.id.tv_wod_item_content);
        }

        void bind(int listIndex) {
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                }
            });
        }
    }
}
