package com.example.bitbybit.xfmproject.dao.contract;

import android.provider.BaseColumns;

/**
 * Created by filipelobo on 09/06/2017.
 */

public class UsuarioContract {
    public static final class UsuarioEntry implements BaseColumns {
        public static final String TABLE_NAME = "usuario";
        public static final String COLUMN_ID_PESSOA = "id_pessoa";
        public static final String COLUMN_USUARIO = "usuario";
        public static final String COLUMN_IS_ATIVO = "is_ativo";
        public static final String COLUMN_IS_PROFESSOR = "is_professor";
    }
}
