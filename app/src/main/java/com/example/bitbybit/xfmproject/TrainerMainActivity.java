package com.example.bitbybit.xfmproject;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.example.bitbybit.xfmproject.dao.contract.PersonalRecordsContract;
import com.example.bitbybit.xfmproject.dao.contract.PessoaContract;
import com.example.bitbybit.xfmproject.dao.contract.TreinoContract;
import com.example.bitbybit.xfmproject.dao.contract.UsuarioContract;
import com.example.bitbybit.xfmproject.dao.contract.WebtokenContract;
import com.example.bitbybit.xfmproject.dao.helper.CrossFDbHelper;
import com.example.bitbybit.xfmproject.domain.PersonalRecords;
import com.example.bitbybit.xfmproject.domain.Pessoa;
import com.example.bitbybit.xfmproject.domain.Treino;
import com.example.bitbybit.xfmproject.domain.Usuario;
import com.example.bitbybit.xfmproject.domain.enums.Box;
import com.example.bitbybit.xfmproject.domain.enums.LPO;
import com.example.bitbybit.xfmproject.utils.CircleTransform;
import com.example.bitbybit.xfmproject.webservice.XFManagerWSClient;
import com.google.gson.Gson;
import com.loopj.android.http.RequestParams;
import com.loopj.android.http.TextHttpResponseHandler;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import cz.msebera.android.httpclient.Header;

public class TrainerMainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private View navHeader;
    private ImageView imgNavHeaderBg, imgProfile;
    private TextView txtName, txtBox;
    private NavigationView navigationView;
    private View mProgressView;
    private View mSaveWodView, mSavedWodView;
    private Spinner mSpinner;
    private Button mBttnSaveWod, mBttnChangeWod;
    private Pessoa mPessoa;
    private Treino mTreino;
    private EditText mETWodPrincipal, mETWodChallenge, mETWodExtra;
    private TextView mDateSavedWOD, mTypeSavedWOD, mPrincipalSavedWOD,
            mChallengeSavedWOD, mExtraSavedWOD;
    private CrossFDbHelper mCrossFDbHelper;

    private static final String urlProfileImg = "http://www.wheretotonight.com/melbourne/images/empty_profile.png";

    @Override
    protected void onStart() {
        super.onStart();

        mCrossFDbHelper = new CrossFDbHelper(this);
        ArrayList<Usuario> arrayList = getWebtokenFromDb();

        if(!checkPersonalInformationOnDb()) {
            Usuario usuario = arrayList.get(0);
            getUserInformationFromWeb(usuario);
        } else {
            mPessoa = getPersonalInformationFromDb();
            Log.d("FROMDB", "Got User from DB!!!");
            loadNavHeader();
            if(checkIfDbHasWODEntries() != null) {
                mTreino = getTreinoFromDb();
                checkFocusedView();
            } else {
                attemptToFetchWOD();
            }
        }
    }

    private Pessoa getPersonalInformationFromDb() {
        SQLiteDatabase db = mCrossFDbHelper.getReadableDatabase();
        String query = "SELECT * FROM " + PessoaContract.PessoaEntry.TABLE_NAME;
        Pessoa pessoa = new Pessoa();
        try {
            Cursor cursor = db.rawQuery(query, null);
            cursor.moveToFirst();
            pessoa.setNome(cursor.getString(1));
            pessoa.setIdade(cursor.getInt(2));
            pessoa.setAniversario(cursor.getString(3));
            pessoa.setCelular(cursor.getString(4));
            pessoa.setBox(cursor.getString(5));
            pessoa.setVencimento(cursor.getString(6));
            pessoa.setPontos(cursor.getInt(7));

            query = "SELECT * FROM " + UsuarioContract.UsuarioEntry.TABLE_NAME;
            cursor = db.rawQuery(query, null);
            cursor.moveToFirst();
            Usuario usuario = new Usuario();
            usuario.setUsuario(cursor.getString(1));
            usuario.setIdPessoa(cursor.getInt(2));
            usuario.setAtivo(cursor.getInt(3) != 0 ? true : false);
            usuario.setProfessor(cursor.getInt(4) != 0 ? true : false);

            query = "SELECT * FROM " + PersonalRecordsContract.PersonalRecordsEntry.TABLE_NAME;
            PersonalRecords prs = new PersonalRecords();
            Map<String, Integer> prsValues = new HashMap<String, Integer>();
            cursor = db.rawQuery(query, null);
            cursor.moveToFirst();
            prs.setId(cursor.getInt(0));
            prsValues.put(LPO.SNATCH.getNome(), cursor.getInt(1));
            prsValues.put(LPO.SQUATSNATCH.getNome(), cursor.getInt(2));
            prsValues.put(LPO.POWERSNATCH.getNome(), cursor.getInt(3));
            prsValues.put(LPO.SNATCHBALANCE.getNome(), cursor.getInt(4));
            prsValues.put(LPO.COMPLEXSNATCH.getNome(), cursor.getInt(5));
            prsValues.put(LPO.CLEAN.getNome(), cursor.getInt(6));
            prsValues.put(LPO.SQUATCLEAN.getNome(), cursor.getInt(7));
            prsValues.put(LPO.POWERCLEAN.getNome(), cursor.getInt(8));
            prsValues.put(LPO.COMPLEXCLEAN.getNome(), cursor.getInt(9));
            prsValues.put(LPO.CLEANANDJEARK.getNome(), cursor.getInt(10));
            prs.setConjuntoPRs(prsValues);

            pessoa.setUsuario(usuario);
            pessoa.setPrs(prs);

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            db.close();
        }
        return pessoa;
    }

    private ArrayList<Usuario> getWebtokenFromDb() {
        ArrayList<Usuario> array = new ArrayList<Usuario>();
        String selectQuery = "SELECT * FROM " + UsuarioContract.UsuarioEntry.TABLE_NAME;
        SQLiteDatabase db = mCrossFDbHelper.getReadableDatabase();
        try {
            Cursor cursor = db.rawQuery(selectQuery, null);
            try {
                if(cursor.moveToFirst()) {
                    do {
                        Usuario usuario = new Usuario();
                        usuario.setUsuario(cursor.getString(1));
                        usuario.setIdPessoa(cursor.getInt(2));
                        usuario.setAtivo(cursor.getInt(3) != 0 ? true : false);
                        usuario.setProfessor(cursor.getInt(4) != 0 ? true : false);
                        array.add(usuario);
                    } while(cursor.moveToNext());
                }
            } finally {
                try {
                    cursor.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } finally {
            try {
                db.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return array;
    }

    private boolean checkPersonalInformationOnDb() {
        String selectionQuery = "SELECT * FROM " + PessoaContract.PessoaEntry.TABLE_NAME;
        SQLiteDatabase db = mCrossFDbHelper.getReadableDatabase();
        try {
            Cursor cursor = db.rawQuery(selectionQuery, null);
            if(cursor.getCount() > 0) {
                cursor.close();
                db.close();
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    private void getUserInformationFromWeb(Usuario usuario) {
        try {

            String urlbodyString = String.format("usuario/carregarinformacoes/" + usuario.getIdPessoa());

            XFManagerWSClient.get(urlbodyString, null, new TextHttpResponseHandler() {

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    Log.d("RESPONSE STRING", responseString);
                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, String responseString) {
                    if(!(responseString.isEmpty())){
                        Log.d("RESPONSE STRING", responseString);
                        try {
                            Gson gson = new Gson();
                            mPessoa = gson.fromJson(responseString, Pessoa.class);
                            savePersonalInformationOnDb(mPessoa);
                            Log.d("FROMWEB", "Got User from Web!");
                            loadNavHeader();
                            if(checkIfDbHasWODEntries() != null) {
                                mTreino = getTreinoFromDb();
                                checkFocusedView();
                            } else {
                                attemptToFetchWOD();
                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } else {
                        System.out.println("User Information empty!");
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void savePersonalInformationOnDb(Pessoa pessoa) {
        SQLiteDatabase db = mCrossFDbHelper.getWritableDatabase();

        ContentValues contentValues = new ContentValues();
        contentValues.put(PessoaContract.PessoaEntry.COLUMN_NOME, pessoa.getNome());
        contentValues.put(PessoaContract.PessoaEntry.COLUMN_IDADE, pessoa.getIdade());
        contentValues.put(PessoaContract.PessoaEntry.COLUMN_ANIVERSARIO, pessoa.getAniversario());
        contentValues.put(PessoaContract.PessoaEntry.COLUMN_CELULAR, pessoa.getCelular());
        contentValues.put(PessoaContract.PessoaEntry.COLUMN_BOX, pessoa.getBox());
        contentValues.put(PessoaContract.PessoaEntry.COLUMN_VENCIMENTO, pessoa.getVencimento());
        contentValues.put(PessoaContract.PessoaEntry.COLUMN_PONTOS, pessoa.getPontos());

        long newPessoaId = db.insert(PessoaContract.PessoaEntry.TABLE_NAME, null, contentValues);

        PersonalRecords prs = pessoa.getPrs();
        contentValues = new ContentValues();
        contentValues.put(PersonalRecordsContract.PersonalRecordsEntry._ID, pessoa.getId());
        if(prs != null && prs.getConjuntoPRs() != null) {
            contentValues.put(PersonalRecordsContract.PersonalRecordsEntry.COLUMN_SNATCH, mapValueOrEmpty(prs.getConjuntoPRs(), LPO.SNATCH));
            contentValues.put(PersonalRecordsContract.PersonalRecordsEntry.COLUMN_SQUATSNATCH, mapValueOrEmpty(prs.getConjuntoPRs(), LPO.SQUATSNATCH));
            contentValues.put(PersonalRecordsContract.PersonalRecordsEntry.COLUMN_POWERSNATCH, mapValueOrEmpty(prs.getConjuntoPRs(), LPO.POWERSNATCH));
            contentValues.put(PersonalRecordsContract.PersonalRecordsEntry.COLUMN_SNATCHBALANCE, mapValueOrEmpty(prs.getConjuntoPRs(), LPO.SNATCHBALANCE));
            contentValues.put(PersonalRecordsContract.PersonalRecordsEntry.COLUMN_COMPLEXSNATCH, mapValueOrEmpty(prs.getConjuntoPRs(), LPO.COMPLEXSNATCH));
            contentValues.put(PersonalRecordsContract.PersonalRecordsEntry.COLUMN_CLEAN, mapValueOrEmpty(prs.getConjuntoPRs(), LPO.CLEAN));
            contentValues.put(PersonalRecordsContract.PersonalRecordsEntry.COLUMN_SQUATCLEAN, mapValueOrEmpty(prs.getConjuntoPRs(), LPO.SQUATCLEAN));
            contentValues.put(PersonalRecordsContract.PersonalRecordsEntry.COLUMN_POWERCLEAN, mapValueOrEmpty(prs.getConjuntoPRs(), LPO.POWERCLEAN));
            contentValues.put(PersonalRecordsContract.PersonalRecordsEntry.COLUMN_COMPLEXCLEAN, mapValueOrEmpty(prs.getConjuntoPRs(), LPO.COMPLEXCLEAN));
            contentValues.put(PersonalRecordsContract.PersonalRecordsEntry.COLUMN_CLEANANDJERK, mapValueOrEmpty(prs.getConjuntoPRs(), LPO.CLEANANDJEARK));
        } else {
            contentValues.put(PersonalRecordsContract.PersonalRecordsEntry.COLUMN_SNATCH, 0);
            contentValues.put(PersonalRecordsContract.PersonalRecordsEntry.COLUMN_SQUATSNATCH, 0);
            contentValues.put(PersonalRecordsContract.PersonalRecordsEntry.COLUMN_POWERSNATCH, 0);
            contentValues.put(PersonalRecordsContract.PersonalRecordsEntry.COLUMN_SNATCHBALANCE, 0);
            contentValues.put(PersonalRecordsContract.PersonalRecordsEntry.COLUMN_COMPLEXSNATCH, 0);
            contentValues.put(PersonalRecordsContract.PersonalRecordsEntry.COLUMN_CLEAN, 0);
            contentValues.put(PersonalRecordsContract.PersonalRecordsEntry.COLUMN_SQUATCLEAN, 0);
            contentValues.put(PersonalRecordsContract.PersonalRecordsEntry.COLUMN_POWERCLEAN, 0);
            contentValues.put(PersonalRecordsContract.PersonalRecordsEntry.COLUMN_COMPLEXCLEAN, 0);
            contentValues.put(PersonalRecordsContract.PersonalRecordsEntry.COLUMN_CLEANANDJERK, 0);
        }

        long newPrsId = db.insert(PersonalRecordsContract.PersonalRecordsEntry.TABLE_NAME, null, contentValues);

        db.close();
    }

    private int mapValueOrEmpty(Map<String, Integer> map, LPO exercicio) {
        Integer value = map.get(exercicio.getNome());
        return value == null ? 0 : value;
    }

    private void attemptToFetchWOD() {
        String bodyUrl = String.format("treino/treinododia/" + mPessoa.getBox());
        Log.d("BODYURL", bodyUrl);
        if(!(bodyUrl.isEmpty())) {
            XFManagerWSClient.get(bodyUrl, null, new TextHttpResponseHandler() {

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    Log.d("FAILURE", responseString);
                    checkFocusedView();
                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, String responseString) {
                    Log.d("SUCCESS", responseString);
                    if(!responseString.isEmpty()) {
                        Gson gson = new Gson();
                        try{
                            mTreino = gson.fromJson(responseString, Treino.class);
                            saveTreinoOnDb(mTreino);
                            checkFocusedView();
                        } catch(Exception e) {
                            checkFocusedView();
                        }
                    }
                }
            });
        }
    }

    private void checkFocusedView() {
        if(mTreino == null) {
            showProgressToEmptyWOD(false);
        } else {
            instantiateSavedWODView(mTreino);
            showProgressToSavedWOD(false);
        }
    }

    private void instantiateSavedWODView(Treino treino) {
        loadSavedWODContentView();
        mDateSavedWOD.setText(String.format("Salvo em ") + treino.getData());
        mTypeSavedWOD.setText(treino.isTipo() ? "Técnica" : "Força");
        mPrincipalSavedWOD.setText(treino.getWodPrincipal());
        mChallengeSavedWOD.setText(treino.getWodDesafio());
        mExtraSavedWOD.setText(treino.getWodExtra());
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        loadMainNavigation();
        loadContentView();
        loadNavHeader();
    }

    private void loadMainNavigation() {
        setContentView(R.layout.activity_trainer_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.trainer_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    private void loadContentView() {
        mProgressView = findViewById(R.id.save_wod_progress);
        mSaveWodView = findViewById(R.id.layout_save_wod);
        mSavedWodView = findViewById(R.id.show_saved_wod_layout);

        loadSaveWODContentView();
        loadSavedWODContentView();
    }

    private void loadSaveWODContentView() {
        mSpinner = (Spinner) findViewById(R.id.spin_wod_types);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.types_challenge_wod, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mSpinner.setAdapter(adapter);

        mETWodPrincipal = (EditText) findViewById(R.id.et_wod_principal);
        mETWodChallenge = (EditText) findViewById(R.id.et_wod_challenge);
        mETWodExtra = (EditText) findViewById(R.id.et_wod_extra);

        mBttnSaveWod = (Button) findViewById(R.id.bttn_save_wod);
        mBttnSaveWod.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                attemptSaveWod();
            }
        });

        mBttnChangeWod = (Button) findViewById(R.id.bttn_trainer_change_wod);
        mBttnChangeWod.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
    }

    private void loadSavedWODContentView() {
        mDateSavedWOD = (TextView) findViewById(R.id.tv_date_saved_wod);
        mTypeSavedWOD = (TextView) findViewById(R.id.tv_type_saved_wod);
        mPrincipalSavedWOD = (TextView) findViewById(R.id.tv_principal_saved_wod);
        mChallengeSavedWOD = (TextView) findViewById(R.id.tv_challenge_saved_wod);
        mExtraSavedWOD = (TextView) findViewById(R.id.tv_extra_saved_wod);
    }

    private void attemptSaveWod() {

        String wodPrincipal = mETWodPrincipal.getText().toString();
        String wodChallenge = mETWodChallenge.getText().toString();
        String wodExtra = mETWodExtra.getText().toString();

        boolean cancel = false;
        View focusView = null;

        if (TextUtils.isEmpty(wodPrincipal)) {
            mETWodPrincipal.setError(getString(R.string.error_field_required));
            focusView = mETWodPrincipal;
            cancel = true;
        }

        if (TextUtils.isEmpty(wodChallenge)) {
            mETWodChallenge.setError(getString(R.string.error_field_required));
            focusView = mETWodChallenge;
            cancel = true;
        }

        if(cancel) {
            focusView.requestFocus();
        } else {
            showProgressToEmptyWOD(true);
            requestToSaveWOD();
        }

    }

    private void requestToSaveWOD() {
        String selectedType = mSpinner.getSelectedItem().toString();

        final Treino treino = new Treino();
        treino.setUsuario(mPessoa.getUsuario());
        if(mPessoa.getBox().equals(Box.Superacao)) {
            treino.setBox(Box.Superacao);
        }
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date date = new Date();
        treino.setData(dateFormat.format(date));
        treino.setWodPrincipal(String.format(selectedType + ": "
                + mETWodPrincipal.getText().toString()));
        treino.setWodDesafio(mETWodChallenge.getText().toString());
        treino.setWodExtra(mETWodExtra.getText().toString());
        if(selectedType.equals("Técnica")) {
            treino.setTipo(true);
        } else {
            treino.setTipo(false);
        }
        treino.setUsuario(mPessoa.getUsuario());

        Gson gson = new Gson();
        String treinoJson = gson.toJson(treino);

        String bodyUrl = "treino/cadastrartreino";
        RequestParams params = new RequestParams();
        params.put("treino", treinoJson);

        XFManagerWSClient.post(bodyUrl, params, new TextHttpResponseHandler() {

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                Log.d("FAILURE", responseString);
                showProgressToEmptyWOD(false);
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseString) {
                Log.d("SUCCESS", responseString);
                mTreino = treino;
                checkFocusedView();
            }
        });
    }

    private void loadNavHeader() {
        navHeader = navigationView.getHeaderView(0);
        txtName = (TextView) navHeader.findViewById(R.id.trainer_name);
        txtBox = (TextView) navHeader.findViewById(R.id.trainer_box);
        // imgNavHeaderBg = (ImageView) navHeader.findViewById(R.id.trainer_img_header_bg);
        //imgProfile = (ImageView) navHeader.findViewById(R.id.trainer_img_profile);

        /**
        Glide.with(this).load(urlProfileImg)
                .crossFade()
                .thumbnail(0.5f)
                .bitmapTransform(new CircleTransform(this))
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(imgProfile);
         **/
        try {
            txtName.setText(String.format(mPessoa.getNome() + ", " + mPessoa.getIdade()));
            txtBox.setText(mPessoa.getBox());
        } catch (Exception e) {
            e.printStackTrace();
            txtName.setText("Username");
            txtBox.setText("Box");
        }
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgressToEmptyWOD(final boolean show) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            mSaveWodView.setVisibility(show ? View.GONE : View.VISIBLE);
            mSaveWodView.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mSaveWodView.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mSaveWodView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgressToSavedWOD(final boolean show) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            mSavedWodView.setVisibility(show ? View.GONE : View.VISIBLE);
            mSavedWodView.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mSavedWodView.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mSavedWodView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }


    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.trainer_logout) {
            AlertDialog.Builder builder;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                builder = new AlertDialog.Builder(this, R.style.Theme_AppCompat_Light_Dialog_Alert);
            } else {
                builder = new AlertDialog.Builder(this);
            }
            builder.setTitle("Logout")
                    .setMessage("Tem certeza que deseja fazer logout?")
                    .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            SQLiteDatabase db = mCrossFDbHelper.getWritableDatabase();
                            db.delete(WebtokenContract.WebtokenEntry.TABLE_NAME, "1", null);
                            db.delete(UsuarioContract.UsuarioEntry.TABLE_NAME, "1", null);
                            db.delete(PessoaContract.PessoaEntry.TABLE_NAME, "1", null);
                            db.delete(PersonalRecordsContract.PersonalRecordsEntry.TABLE_NAME, "1", null);
                            db.delete(TreinoContract.TreinoEntry.TABLE_NAME, "1", null);

                            Intent intent = new Intent(getBaseContext(), LoginActivity.class);
                            startActivity(intent);
                            finish();
                        }
                    })
                    .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            // Do nothing
                        }
                    })
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .show();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private Treino getTreinoFromDb() {
        ArrayList<Treino> array = new ArrayList<Treino>();
        String selectQuery = "SELECT * FROM " + TreinoContract.TreinoEntry.TABLE_NAME;
        SQLiteDatabase db = mCrossFDbHelper.getReadableDatabase();
        try {
            Cursor cursor = db.rawQuery(selectQuery, null);
            try {
                if(cursor.moveToFirst()) {
                    do {
                        Treino treino = new Treino();
                        treino.setBox(cursor.getString(1));
                        treino.getUsuario().setUsuario(cursor.getString(2));
                        treino.setData(cursor.getString(3));
                        treino.setWodPrincipal(cursor.getString(4));
                        treino.setWodDesafio(cursor.getString(5));
                        treino.setWodExtra(cursor.getString(6));
                        treino.setTipo(cursor.getInt(7) != 0 ? true : false);
                        array.add(treino);
                    } while(cursor.moveToNext());
                }
            } finally {
                try {
                    cursor.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } finally {
            try {
                db.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return array.get(0);
    }

    private Cursor checkIfDbHasWODEntries() {
        String selectionQuery = "SELECT * FROM " + TreinoContract.TreinoEntry.TABLE_NAME;
        SQLiteDatabase db = mCrossFDbHelper.getReadableDatabase();
        try {
            Cursor cursor = db.rawQuery(selectionQuery, null);
            if(cursor.getCount() > 0) {
                db.close();
                return cursor;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    private long saveTreinoOnDb(Treino treino) {
        SQLiteDatabase db = mCrossFDbHelper.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(TreinoContract.TreinoEntry.COLUMN_BOX, treino.getBox().getNome());
        values.put(TreinoContract.TreinoEntry.COLUMN_USUARIO, treino.getUsuario() != null ? treino.getUsuario().getUsuario() : "");
        values.put(TreinoContract.TreinoEntry.COLUMN_DATA, treino.getData());
        values.put(TreinoContract.TreinoEntry.COLUMN_WOD_PRINCIPAL, treino.getWodPrincipal());
        values.put(TreinoContract.TreinoEntry.COLUMN_WOD_DESAFIO, treino.getWodDesafio());
        values.put(TreinoContract.TreinoEntry.COLUMN_WOD_EXTRA, treino.getWodExtra());
        values.put(TreinoContract.TreinoEntry.COLUMN_TIPO, treino.isTipo());

        long newRowId = db.insert(TreinoContract.TreinoEntry.TABLE_NAME, null, values);
        Log.d("ID", String.valueOf(newRowId));
        db.close();
        return newRowId;
    }
}
