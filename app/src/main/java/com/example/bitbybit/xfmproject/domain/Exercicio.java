package com.example.bitbybit.xfmproject.domain;

import com.example.bitbybit.xfmproject.domain.enums.LPO;
import com.example.bitbybit.xfmproject.domain.enums.Soldiers;

/**
 * Created by filipelobo on 27/06/2017.
 */

public class Exercicio {

    private String exercicio;
    private String repeticoes;
    private int minutos;

    public String getExercicio() {
        return exercicio;
    }

    public void setExercicio(String exercicio) {
        this.exercicio = exercicio;
    }

    public String getRepeticoes() {
        return repeticoes;
    }

    public void setRepeticoes(String repeticoes) {
        this.repeticoes = repeticoes;
    }

    public int getMinutos() {
        return minutos;
    }

    public void setMinutos(int minutos) {
        this.minutos = minutos;
    }

    /**
    private Enum exercicio;
    private String repeticoes;
    private int minutos;

    public Exercicio(Soldiers exercicio) {
        this.exercicio = exercicio;
        this.repeticoes = exercicio.getDetalhes();
        this.minutos = exercicio.getMinutos();
    }

    public Exercicio(LPO exercicio, String repeticoes) {
        this.exercicio = exercicio;
        this.repeticoes = repeticoes;

    }

    public Enum getExercicio() {
        return exercicio;
    }

    public void setExercicio(Enum exercicio) {
        this.exercicio = exercicio;
    }

    public String getRepeticoes() {
        return repeticoes;
    }

    public void setRepeticoes(String repeticoes) {
        this.repeticoes = repeticoes;
    }

    public int getMinutos() {
        return minutos;
    }

    public void setMinutos(int minutos) {
        this.minutos = minutos;
    }
     **/
}
