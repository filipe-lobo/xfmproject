package com.example.bitbybit.xfmproject;

import android.annotation.TargetApi;
import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.Message;
import android.provider.SyncStateContract;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Chronometer;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;
import com.example.bitbybit.xfmproject.controller.BattleOptionsAdapter;
import com.example.bitbybit.xfmproject.controller.BluetoothConstants;
import com.example.bitbybit.xfmproject.controller.BluetoothService;
import com.example.bitbybit.xfmproject.domain.BluetoothObject;
import com.example.bitbybit.xfmproject.domain.Exercicio;
import com.example.bitbybit.xfmproject.domain.Pessoa;
import com.example.bitbybit.xfmproject.webservice.XFManagerWSClient;
import com.google.gson.Gson;
import com.loopj.android.http.TextHttpResponseHandler;

import java.util.concurrent.TimeUnit;

import cz.msebera.android.httpclient.Header;


public class BattleFragment extends Fragment implements ActivityCompat.OnRequestPermissionsResultCallback,
        BattleOptionsAdapter.OnBattleOptionsItemClickListener {
    private static final String TAG = "BattleFragment";

    private static final int REQUEST_CONNECT_DEVICE_SECURE = 1;
    private static final int REQUEST_ENABLE_BT = 3;

    private View mRootView;
    private Button mConnectDeviceButton;
    private ScrollView mChooseBattleWodLinearLayout;
    private LinearLayout mBattleRunningLinearLayout;
    private TextView mBattleRunningExerciseTextView, mBattleRunningRepetitionsTextView;
    private TextView mChronometer;
    private ImageButton mStartChronometer, mStopChronometer;
    private RecyclerView mBattleWodRecyclerView;
    private BattleOptionsAdapter mBattleOptionsAdapter;
    private Button mStartBattleButton, mFinishBattleButton;
    private boolean isChronometerPaused = true;
    private boolean isBattleStarted = false;
    private long timeRemaininginChronometer = 0;
    private int mAdversaryId = -1;

    private Pessoa mPessoa;
    private Exercicio mExercicio;
    private BluetoothObject mBluetoothObject;
    private Exercicio[] mBattleOptionsList;

    private String mConnectedDeviceName = null;
    private BluetoothAdapter mBluetoothAdapter = null;
    private BluetoothService mBluetoothService = null;


    public BattleFragment() {

    }

    public static BattleFragment newInstance(Pessoa pessoa) {
        BattleFragment fragment = new BattleFragment();
        fragment.mPessoa = pessoa;
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mBluetoothService != null) {
            mBluetoothService.stop();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mBluetoothService != null) {
            if (mBluetoothService.getState() == BluetoothService.STATE_NONE) {
                mBluetoothService.start();
            }
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mRootView = inflater.inflate(R.layout.fragment_battle, container, false);
        return mRootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        setBattleLayout(view);
    }

    private void setBattleLayout(View view) {
        mConnectDeviceButton = (Button) view.findViewById(R.id.bttn_battle_fragment_connect);
        mConnectDeviceButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!mBluetoothAdapter.isEnabled()) {
                    Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                    startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
                } else {
                    if (mBluetoothService == null) {
                        mBluetoothService = new BluetoothService(getActivity(), mHandler);
                    }
                    ensureDiscoverable();
                }
            }
        });

        mBattleWodRecyclerView = (RecyclerView) view.findViewById(R.id.recyclerview_battle_wod_options);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(view.getContext(), LinearLayoutManager.VERTICAL, false);
        mBattleWodRecyclerView.setLayoutManager(linearLayoutManager);
        mBattleWodRecyclerView.setHasFixedSize(true);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(mBattleWodRecyclerView.getContext(),
                linearLayoutManager.getOrientation());
        mBattleWodRecyclerView.addItemDecoration(dividerItemDecoration);
        mStartBattleButton = (Button) view.findViewById(R.id.bttn_battle_fragment_start);
        mStartBattleButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tryToStartBattle();
            }
        });
        mChooseBattleWodLinearLayout = (ScrollView) view.findViewById(R.id.choose_battle_wod_linear_layout);

        mBattleRunningExerciseTextView = (TextView) view.findViewById(R.id.tv_battle_running_exercise);
        mBattleRunningRepetitionsTextView = (TextView) view.findViewById(R.id.tv_battle_running_repetitions);
        mChronometer = (TextView) view.findViewById(R.id.countdown_chronometer);
        mStartChronometer = (ImageButton) view.findViewById(R.id.start_chronometer);
        mStartChronometer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mBluetoothObject.setType(BluetoothConstants.TYPE_CHRONOMETER_START);
                if (mBluetoothService.getState() != BluetoothService.STATE_CONNECTED) {
                    Toast.makeText(getActivity(), R.string.not_connected, Toast.LENGTH_SHORT).show();
                    return;
                }
                Gson gson = new Gson();
                String message = gson.toJson(mBluetoothObject);
                if (message.length() > 0) {
                    byte[] send = message.getBytes();
                    mBluetoothService.write(send);
                }
            }
        });
        mStopChronometer = (ImageButton) view.findViewById(R.id.stop_chronometer);
        mStopChronometer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mBluetoothObject.setType(BluetoothConstants.TYPE_CHRONOMETER_PAUSE);
                if (mBluetoothService.getState() != BluetoothService.STATE_CONNECTED) {
                    Toast.makeText(getActivity(), R.string.not_connected, Toast.LENGTH_SHORT).show();
                    return;
                }
                Gson gson = new Gson();
                String message = gson.toJson(mBluetoothObject);
                if (message.length() > 0) {
                    byte[] send = message.getBytes();
                    mBluetoothService.write(send);
                }
            }
        });
        mFinishBattleButton = (Button) view.findViewById(R.id.bttn_battle_running_finish_battle);
        mFinishBattleButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder;
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    builder = new AlertDialog.Builder(getContext(), R.style.Theme_AppCompat_Light_Dialog_Alert);
                } else {
                    builder = new AlertDialog.Builder(getContext());
                }
                builder.setTitle("Finalizar Batalha")
                        .setMessage("Deseja finalizar a batalha?")
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                finishBattle();
                            }
                        })
                        .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {

                            }
                        });
                AlertDialog alertDialog = builder.create();
                alertDialog.show();
            }
        });
        mBattleRunningLinearLayout = (LinearLayout) view.findViewById(R.id.battle_running_linear_layout);
        setRecyclerViewData();
    }

    private void setVisibilites(int connectDeviceVisibility, int chooseWodVisibility, int battleRunningVisibility) {
        mConnectDeviceButton.setVisibility(connectDeviceVisibility);
        mChooseBattleWodLinearLayout.setVisibility(chooseWodVisibility);
        mBattleRunningLinearLayout.setVisibility(battleRunningVisibility);
    }

    private void finishBattle() {

        String url = String.format(mPessoa.getUsuario().getIdPessoa() + "/" + mAdversaryId);

        XFManagerWSClient.post(url, null, new TextHttpResponseHandler() {
            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                Log.e("FAILURE", responseString);
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseString) {
                Log.e("SUCCESS", responseString);
                mBluetoothObject.setType(BluetoothConstants.TYPE_FINISH_BATTLE);
                if (mBluetoothService.getState() != BluetoothService.STATE_CONNECTED) {
                    Toast.makeText(getActivity(), R.string.not_connected, Toast.LENGTH_SHORT).show();
                    return;
                }
                Gson gson = new Gson();
                String message = gson.toJson(mBluetoothObject);
                if (message.length() > 0) {
                    byte[] send = message.getBytes();
                    mBluetoothService.write(send);
                }
            }
        });
    }

    private void startChronometerButtonClicked () {
        isChronometerPaused = false;

        mStartChronometer.setEnabled(false);
        mStopChronometer.setEnabled(true);

        CountDownTimer timer;
        if(mBluetoothObject != null && timeRemaininginChronometer == 0) {
            if(!isBattleStarted) {
                timeRemaininginChronometer = (mBluetoothObject.getExercicio().getMinutos() * 60000);
                isBattleStarted = true;
            } else {
                isBattleStarted = false;
            }
        }
        long millisInFuture = timeRemaininginChronometer;
        final long countDownInterval = 1000;

        timer = new CountDownTimer(millisInFuture,countDownInterval){
            public void onTick(long millisUntilFinished){
                if(isChronometerPaused)
                {
                    cancel();
                }
                else {
                    mChronometer.setText(String.format("%02d:%02d",
                            TimeUnit.MILLISECONDS.toMinutes( millisUntilFinished),
                            TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) -
                                    TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished))));
                    timeRemaininginChronometer = millisUntilFinished;
                }
            }
            public void onFinish(){
                mChronometer.setText("O tempo acabou!");
                mStartChronometer.setEnabled(false);
                mStopChronometer.setEnabled(false);
                mFinishBattleButton.setEnabled(false);
            }
        }.start();
    }

    private void stopChronometerButtonClicked() {
        isChronometerPaused = true;

        mStartChronometer.setEnabled(true);
        mStopChronometer.setEnabled(false);
    }

    private void tryToStartBattle() {
        if(mExercicio != null) {
            mBluetoothObject = new BluetoothObject(BluetoothConstants.TYPE_START_BATTLE, mPessoa, mExercicio);

            if (mBluetoothService.getState() != BluetoothService.STATE_CONNECTED) {
                Toast.makeText(getActivity(), R.string.not_connected, Toast.LENGTH_SHORT).show();
                return;
            }

            Gson gson = new Gson();
            String message = gson.toJson(mBluetoothObject);
            if (message.length() > 0) {
                byte[] send = message.getBytes();
                mBluetoothService.write(send);
            }
        }
    }

    private void getRemoteBattleChoices() {
        String url = "modobatalha";
        XFManagerWSClient.get(url, null, new TextHttpResponseHandler() {
            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {

            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseString) {
                if(!responseString.isEmpty()) {
                    try {
                        Gson gson = new Gson();
                        mBattleOptionsList = gson.fromJson(responseString, Exercicio[].class);
                        for(Exercicio e : mBattleOptionsList) {
                            System.out.println(e.getExercicio() + e.getRepeticoes() + e.getMinutos());
                        }
                        setRecyclerViewData();
                    } catch(Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }

    private void setRecyclerViewData() {
        if(mBattleOptionsList != null) {
            mBattleOptionsAdapter = new BattleOptionsAdapter(getContext(), mBattleOptionsList, this);
        } else {
            mBattleOptionsAdapter = new BattleOptionsAdapter(getContext(), new Exercicio[0], this);
        }
        mBattleWodRecyclerView.setAdapter(mBattleOptionsAdapter);
    }

    private void showBattleOptions() {
        mChooseBattleWodLinearLayout.setVisibility(View.VISIBLE);
    }

    private void connectDevice(Intent data, boolean secure) {
        String address = data.getExtras()
                .getString(DeviceListActivity.EXTRA_DEVICE_ADDRESS);
        BluetoothDevice device = mBluetoothAdapter.getRemoteDevice(address);
        mBluetoothService.connect(device, secure);
    }

    private final Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            FragmentActivity activity = getActivity();
            switch (msg.what) {
                case BluetoothConstants.MESSAGE_STATE_CHANGE:
                    switch (msg.arg1) {
                        case BluetoothService.STATE_CONNECTED:
                            getRemoteBattleChoices();
                            if(isBattleStarted && timeRemaininginChronometer > 0) {
                                stopChronometerButtonClicked();
                                setVisibilites(View.GONE, View.GONE, View.VISIBLE);
                            } else if(isBattleStarted && timeRemaininginChronometer == 0) {
                                isBattleStarted = false;
                                setVisibilites(View.GONE, View.VISIBLE, View.GONE);
                            } else {
                                setVisibilites(View.GONE, View.VISIBLE, View.GONE);
                            }
                            break;
                        case BluetoothService.STATE_CONNECTING:
                            setVisibilites(View.VISIBLE, View.GONE, View.GONE);
                            Toast.makeText(activity, "Conectando...", Toast.LENGTH_SHORT).show();
                            break;
                        case BluetoothService.STATE_LISTEN:
                            setVisibilites(View.VISIBLE, View.GONE, View.GONE);
                        case BluetoothService.STATE_NONE:
                            setVisibilites(View.VISIBLE, View.GONE, View.GONE);;
                            break;
                    }
                    break;
                case BluetoothConstants.MESSAGE_WRITE:
                    byte[] writeBuf = (byte[]) msg.obj;
                    String writeMessage = new String(writeBuf);

                    BluetoothObject bluetoothObject = new Gson().fromJson(writeMessage, BluetoothObject.class);
                    switch (bluetoothObject.getType()) {
                        case BluetoothConstants.TYPE_ACCEPT_BATTLE: {
                            mAdversaryId = bluetoothObject.getIdPessoa();
                        }
                        case BluetoothConstants.TYPE_CHRONOMETER_START: {
                            startChronometerButtonClicked();
                        }
                        case BluetoothConstants.TYPE_CHRONOMETER_PAUSE: {
                            stopChronometerButtonClicked();
                        }
                        case BluetoothConstants.TYPE_FINISH_BATTLE: {

                        }
                    }

                    break;
                case BluetoothConstants.MESSAGE_READ:
                    byte[] readBuf = (byte[]) msg.obj;
                    String readMessage = new String(readBuf, 0, msg.arg1);
                    Log.e("JSON", readMessage);
                    try {
                        Gson gson = new Gson();
                        mBluetoothObject = gson.fromJson(readMessage, BluetoothObject.class);
                        switch (mBluetoothObject.getType()) {
                            case BluetoothConstants.TYPE_START_BATTLE : {
                                tryToAcceptBattle();
                            }
                            case BluetoothConstants.TYPE_ACCEPT_BATTLE: {
                                startAndShowRunningBattle();
                            }
                            case BluetoothConstants.TYPE_CHRONOMETER_START: {
                                startChronometerButtonClicked();
                            }
                            case BluetoothConstants.TYPE_CHRONOMETER_PAUSE: {
                                stopChronometerButtonClicked();
                            }
                            case BluetoothConstants.TYPE_FINISH_BATTLE: {

                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    break;
                case BluetoothConstants.MESSAGE_DEVICE_NAME:
                    mConnectedDeviceName = msg.getData().getString(BluetoothConstants.DEVICE_NAME);
                    if (null != activity) {
                        Toast.makeText(activity, "Conectado a "
                                + mConnectedDeviceName, Toast.LENGTH_SHORT).show();
                    }
                    showBattleOptions();
                    break;
                case BluetoothConstants.MESSAGE_TOAST:
                    if (null != activity) {
                        Toast.makeText(activity, msg.getData().getString(BluetoothConstants.TOAST),
                                Toast.LENGTH_SHORT).show();
                    }
                    break;
            }
        }
    };

    private void tryToAcceptBattle() {
        AlertDialog.Builder builder;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder = new AlertDialog.Builder(getContext(), R.style.Theme_AppCompat_Light_Dialog_Alert);
        } else {
            builder = new AlertDialog.Builder(getContext());
        }
        builder.setTitle("Solicitação de Batalha!")
                .setMessage("O usuário " + mBluetoothObject.getNomePessoa()
                        + " deseja batalhar o treino " + mBluetoothObject.getExercicio().getExercicio()
                        + " com você. Deseja aceitar?")
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        mBluetoothObject = new BluetoothObject(BluetoothConstants.TYPE_ACCEPT_BATTLE, mPessoa, mBluetoothObject.getExercicio());
                        sendAcceptingObject();
                    }
                })
                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    private void sendAcceptingObject() {
        if (mBluetoothService.getState() != BluetoothService.STATE_CONNECTED) {
            Toast.makeText(getActivity(), R.string.not_connected, Toast.LENGTH_SHORT).show();
            return;
        }
        Gson gson = new Gson();
        String message = gson.toJson(mBluetoothObject);
        if (message.length() > 0) {
            byte[] send = message.getBytes();
            mBluetoothService.write(send);
        }
        startAndShowRunningBattle();
    }

    private void startAndShowRunningBattle() {
        mChooseBattleWodLinearLayout.setVisibility(View.GONE);
        mBattleRunningExerciseTextView.setText(mBluetoothObject.getExercicio().getExercicio());
        mBattleRunningRepetitionsTextView.setText(mBluetoothObject.getExercicio().getRepeticoes());
        mBattleRunningLinearLayout.setVisibility(View.VISIBLE);
    }

    private void ensureDiscoverable() {
        if (mBluetoothAdapter.getScanMode() !=
                BluetoothAdapter.SCAN_MODE_CONNECTABLE_DISCOVERABLE) {
            Intent discoverableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE);
            discoverableIntent.putExtra(BluetoothAdapter.EXTRA_DISCOVERABLE_DURATION, 300);
            startActivity(discoverableIntent);
            Intent serverIntent = new Intent(getActivity(), DeviceListActivity.class);
            startActivityForResult(serverIntent, REQUEST_CONNECT_DEVICE_SECURE);
        } else {
            Intent serverIntent = new Intent(getActivity(), DeviceListActivity.class);
            startActivityForResult(serverIntent, REQUEST_CONNECT_DEVICE_SECURE);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case REQUEST_CONNECT_DEVICE_SECURE: {
                if (resultCode == Activity.RESULT_OK) {
                    connectDevice(data, true);
                }
                break;
            }
            case REQUEST_ENABLE_BT: {
                if (resultCode == Activity.RESULT_OK) {
                    if (mBluetoothService == null) {
                        mBluetoothService = new BluetoothService(getActivity(), mHandler);
                    }
                    ensureDiscoverable();
                } else {
                    Log.d(TAG, "BT not enabled");
                    Toast.makeText(getActivity(), R.string.bt_not_enabled,
                            Toast.LENGTH_SHORT).show();
                }
            }
        }
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void onBattleOptionsItemClick(Exercicio exercicio) {
        mExercicio = exercicio;
        if(exercicio != null) {
            mStartBattleButton.setBackground(getContext().getDrawable(R.drawable.button_background));
            mStartBattleButton.setEnabled(true);
        } else {
            mStartBattleButton.setBackground(getContext().getDrawable(R.drawable.secondary_button_background));
            mStartBattleButton.setEnabled(false);
        }
    }
}
