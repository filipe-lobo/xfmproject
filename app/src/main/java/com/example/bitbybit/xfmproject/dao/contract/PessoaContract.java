package com.example.bitbybit.xfmproject.dao.contract;

import android.provider.BaseColumns;

/**
 * Created by filipelobo on 09/06/2017.
 */

public class PessoaContract {
    public static final class PessoaEntry implements BaseColumns {
        public static final String TABLE_NAME = "pessoa";
        public static final String COLUMN_NOME = "nome";
        public static final String COLUMN_IDADE = "idade";
        public static final String COLUMN_ANIVERSARIO = "aniversario";
        public static final String COLUMN_CELULAR = "celular";
        public static final String COLUMN_BOX = "box";
        public static final String COLUMN_VENCIMENTO = "vencimento";
        public static final String COLUMN_PONTOS = "pontos";
    }
}
