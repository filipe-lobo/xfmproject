package com.example.bitbybit.xfmproject.domain;

/**
 * Created by filipelobo on 26/05/2017.
 */

public class Usuario extends GenericDomain {

    private String usuario;
    private String senha;
    private int id_pessoa;
    private boolean isProfessor;
    private boolean isAtivo;

    public Usuario() {

    }

    public Usuario(String usuario, String senha, boolean professor) {
        this.usuario = usuario;
        this.senha = senha;
        this.id_pessoa = -1;
        this.isProfessor = professor;
        this.isAtivo = true;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    public int getIdPessoa() {
        return id_pessoa;
    }

    public void setIdPessoa(int idPessoa) {
        this.id_pessoa = idPessoa;
    }

    public boolean isProfessor() {
        return isProfessor;
    }

    public void setProfessor(boolean professor) {
        this.isProfessor = professor;
    }

    public boolean isAtivo() {
        return isAtivo;
    }

    public void setAtivo(boolean isAtivo) {
        this.isAtivo = isAtivo;
    }

    @Override
    public String toString() {
        return "Usuario{id=" + id + ", usuario=" + usuario + ", senha=" + senha + ", id_pessoa=" + id_pessoa + ", isProfessor=" + isProfessor + ", isAtivo=" + isAtivo + '}';
    }

}
