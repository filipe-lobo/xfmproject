package com.example.bitbybit.xfmproject.dao.contract;

import android.provider.BaseColumns;

/**
 * Created by filipelobo on 02/06/2017.
 */

public class WebtokenContract {
    public static final class WebtokenEntry implements BaseColumns {
        public static final String TABLE_NAME = "webtoken";
        public static final String COLUMN_PRIMARY_KEY_PESSOA = "primarykey_pessoa";
        public static final String COLUMN_USUARIO = "usuario";
        public static final String COLUMN_IS_PROFESSOR = "is_professor";
    }
}
