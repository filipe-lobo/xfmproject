package com.example.bitbybit.xfmproject;

import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.preference.PreferenceActivity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.example.bitbybit.xfmproject.dao.contract.PersonalRecordsContract;
import com.example.bitbybit.xfmproject.dao.contract.PessoaContract;
import com.example.bitbybit.xfmproject.dao.contract.UsuarioContract;
import com.example.bitbybit.xfmproject.dao.contract.WebtokenContract;
import com.example.bitbybit.xfmproject.dao.helper.CrossFDbHelper;
import com.example.bitbybit.xfmproject.domain.PersonalRecords;
import com.example.bitbybit.xfmproject.domain.Pessoa;
import com.example.bitbybit.xfmproject.domain.Usuario;
import com.example.bitbybit.xfmproject.domain.Webtoken;
import com.example.bitbybit.xfmproject.domain.enums.LPO;
import com.example.bitbybit.xfmproject.utils.CircleTransform;
import com.example.bitbybit.xfmproject.webservice.XFManagerWSClient;
import com.google.gson.Gson;
import com.loopj.android.http.TextHttpResponseHandler;

import java.util.ArrayList;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.Map;

import cz.msebera.android.httpclient.Header;

public class PersonalInformationActivity extends AppCompatActivity {

    private ImageView mProfilePicture;
    private TextView mNameAndAgeTextView, mBoxTextView;
    private TextView mPointsTextView;
    private TextView mPhoneTextView, mBirthdayTextView;
    private TextView mChangeUsernameTextView, mChangePasswordTextView;

    private CrossFDbHelper mCrossFDbHelper;
    private Pessoa mPessoa;

    private static final String urlProfileImg = "http://www.wheretotonight.com/melbourne/images/empty_profile.png";

    @Override
    protected void onStart() {
        super.onStart();
        mCrossFDbHelper = new CrossFDbHelper(this);
        if(!checkPersonalInformationOnDb()) {
            ArrayList<Webtoken> arrayList = getWebtokenFromDb();
            if(arrayList.size() < 1) {
                Intent intent = new Intent(this, LoginActivity.class);
                startActivity(intent);
                this.finish();
            }
            Webtoken webtoken = arrayList.get(0);
            getUserInformationFromWeb(webtoken);
        } else {
            mPessoa = getPersonalInformationFromDb();
            Log.d("FROMDB", "Got User from DB!!!");
            setUpPersonalContent(mPessoa);
        }
    }

    private ArrayList<Webtoken> getWebtokenFromDb() {
        ArrayList<Webtoken> array = new ArrayList<Webtoken>();
        String selectQuery = "SELECT * FROM " + WebtokenContract.WebtokenEntry.TABLE_NAME;
        SQLiteDatabase db = mCrossFDbHelper.getReadableDatabase();
        try {
            Cursor cursor = db.rawQuery(selectQuery, null);
            try {
                if(cursor.moveToFirst()) {
                    do {
                        Webtoken webtoken = new Webtoken();
                        webtoken.setPrimarykey_pessoa(cursor.getInt(1));
                        webtoken.setUsuario(cursor.getString(2));
                        webtoken.setProfessor(cursor.getInt(3) != 0 ? true : false);
                        array.add(webtoken);
                    } while(cursor.moveToNext());
                }
            } finally {
                try {
                    cursor.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } finally {
            try {
                db.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return array;
    }

    private boolean checkPersonalInformationOnDb() {
        String selectionQuery = "SELECT * FROM " + PessoaContract.PessoaEntry.TABLE_NAME;
        SQLiteDatabase db = mCrossFDbHelper.getReadableDatabase();
        try {
            Cursor cursor = db.rawQuery(selectionQuery, null);
            if(cursor.getCount() > 0) {
                cursor.close();
                db.close();
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    private Pessoa getPersonalInformationFromDb() {
        SQLiteDatabase db = mCrossFDbHelper.getReadableDatabase();
        String query = "SELECT * FROM " + PessoaContract.PessoaEntry.TABLE_NAME;
        Pessoa pessoa = new Pessoa();
        try {
            Cursor cursor = db.rawQuery(query, null);
            cursor.moveToFirst();
            pessoa.setNome(cursor.getString(1));
            pessoa.setIdade(cursor.getInt(2));
            pessoa.setAniversario(cursor.getString(3));
            pessoa.setCelular(cursor.getString(4));
            pessoa.setBox(cursor.getString(5));
            pessoa.setVencimento(cursor.getString(6));
            pessoa.setPontos(cursor.getInt(7));

            query = "SELECT * FROM " + UsuarioContract.UsuarioEntry.TABLE_NAME;
            cursor = db.rawQuery(query, null);
            cursor.moveToFirst();
            Usuario usuario = new Usuario();
            usuario.setUsuario(cursor.getString(1));
            usuario.setIdPessoa(cursor.getInt(2));
            usuario.setAtivo(cursor.getInt(3) != 0 ? true : false);
            usuario.setProfessor(cursor.getInt(4) != 0 ? true : false);

            query = "SELECT * FROM " + PersonalRecordsContract.PersonalRecordsEntry.TABLE_NAME;
            PersonalRecords prs = new PersonalRecords();
            Map<String, Integer> prsValues = new HashMap<String, Integer>();
            cursor = db.rawQuery(query, null);
            cursor.moveToFirst();
            prs.setId(cursor.getInt(0));
            prsValues.put(LPO.SNATCH.getNome(), cursor.getInt(1));
            prsValues.put(LPO.SQUATSNATCH.getNome(), cursor.getInt(2));
            prsValues.put(LPO.POWERSNATCH.getNome(), cursor.getInt(3));
            prsValues.put(LPO.SNATCHBALANCE.getNome(), cursor.getInt(4));
            prsValues.put(LPO.COMPLEXSNATCH.getNome(), cursor.getInt(5));
            prsValues.put(LPO.CLEAN.getNome(), cursor.getInt(6));
            prsValues.put(LPO.SQUATCLEAN.getNome(), cursor.getInt(7));
            prsValues.put(LPO.POWERCLEAN.getNome(), cursor.getInt(8));
            prsValues.put(LPO.COMPLEXCLEAN.getNome(), cursor.getInt(9));
            prsValues.put(LPO.CLEANANDJEARK.getNome(), cursor.getInt(10));
            prs.setConjuntoPRs(prsValues);

            pessoa.setUsuario(usuario);
            pessoa.setPrs(prs);

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            db.close();
        }
        return pessoa;
    }

    private void getUserInformationFromWeb(Webtoken webtoken) {
        try {

            String urlbodyString = String.format("usuario/carregarinformacoes/" + webtoken.getPrimarykey_pessoa());

            XFManagerWSClient.get(urlbodyString, null, new TextHttpResponseHandler() {

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {

                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, String responseString) {
                    if(!(responseString.isEmpty())){
                        try {
                            Gson gson = new Gson();
                            mPessoa = gson.fromJson(responseString, Pessoa.class);
                            savePersonalInformationOnDb(mPessoa);
                            Log.d("FROMWEB", "Got User from Web!");

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } else {
                        System.out.println("User Information empty!");
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void savePersonalInformationOnDb(Pessoa pessoa) {
        SQLiteDatabase db = mCrossFDbHelper.getWritableDatabase();

        ContentValues contentValues = new ContentValues();
        contentValues.put(PessoaContract.PessoaEntry.COLUMN_NOME, pessoa.getNome());
        contentValues.put(PessoaContract.PessoaEntry.COLUMN_IDADE, pessoa.getIdade());
        contentValues.put(PessoaContract.PessoaEntry.COLUMN_ANIVERSARIO, pessoa.getAniversario());
        contentValues.put(PessoaContract.PessoaEntry.COLUMN_CELULAR, pessoa.getCelular());
        contentValues.put(PessoaContract.PessoaEntry.COLUMN_BOX, pessoa.getBox());
        contentValues.put(PessoaContract.PessoaEntry.COLUMN_VENCIMENTO, pessoa.getVencimento());
        contentValues.put(PessoaContract.PessoaEntry.COLUMN_PONTOS, pessoa.getPontos());

        long newPessoaId = db.insert(PessoaContract.PessoaEntry.TABLE_NAME, null, contentValues);

        PersonalRecords prs = pessoa.getPrs();
        contentValues = new ContentValues();
        contentValues.put(PersonalRecordsContract.PersonalRecordsEntry._ID, pessoa.getId());
        if(prs != null && prs.getConjuntoPRs() != null) {
            contentValues.put(PersonalRecordsContract.PersonalRecordsEntry.COLUMN_SNATCH, mapValueOrEmpty(prs.getConjuntoPRs(), LPO.SNATCH));
            contentValues.put(PersonalRecordsContract.PersonalRecordsEntry.COLUMN_SQUATSNATCH, mapValueOrEmpty(prs.getConjuntoPRs(), LPO.SQUATSNATCH));
            contentValues.put(PersonalRecordsContract.PersonalRecordsEntry.COLUMN_POWERSNATCH, mapValueOrEmpty(prs.getConjuntoPRs(), LPO.POWERSNATCH));
            contentValues.put(PersonalRecordsContract.PersonalRecordsEntry.COLUMN_SNATCHBALANCE, mapValueOrEmpty(prs.getConjuntoPRs(), LPO.SNATCHBALANCE));
            contentValues.put(PersonalRecordsContract.PersonalRecordsEntry.COLUMN_COMPLEXSNATCH, mapValueOrEmpty(prs.getConjuntoPRs(), LPO.COMPLEXSNATCH));
            contentValues.put(PersonalRecordsContract.PersonalRecordsEntry.COLUMN_CLEAN, mapValueOrEmpty(prs.getConjuntoPRs(), LPO.CLEAN));
            contentValues.put(PersonalRecordsContract.PersonalRecordsEntry.COLUMN_SQUATCLEAN, mapValueOrEmpty(prs.getConjuntoPRs(), LPO.SQUATCLEAN));
            contentValues.put(PersonalRecordsContract.PersonalRecordsEntry.COLUMN_POWERCLEAN, mapValueOrEmpty(prs.getConjuntoPRs(), LPO.POWERCLEAN));
            contentValues.put(PersonalRecordsContract.PersonalRecordsEntry.COLUMN_COMPLEXCLEAN, mapValueOrEmpty(prs.getConjuntoPRs(), LPO.COMPLEXCLEAN));
            contentValues.put(PersonalRecordsContract.PersonalRecordsEntry.COLUMN_CLEANANDJERK, mapValueOrEmpty(prs.getConjuntoPRs(), LPO.CLEANANDJEARK));
        } else {
            contentValues.put(PersonalRecordsContract.PersonalRecordsEntry.COLUMN_SNATCH, 0);
            contentValues.put(PersonalRecordsContract.PersonalRecordsEntry.COLUMN_SQUATSNATCH, 0);
            contentValues.put(PersonalRecordsContract.PersonalRecordsEntry.COLUMN_POWERSNATCH, 0);
            contentValues.put(PersonalRecordsContract.PersonalRecordsEntry.COLUMN_SNATCHBALANCE, 0);
            contentValues.put(PersonalRecordsContract.PersonalRecordsEntry.COLUMN_COMPLEXSNATCH, 0);
            contentValues.put(PersonalRecordsContract.PersonalRecordsEntry.COLUMN_CLEAN, 0);
            contentValues.put(PersonalRecordsContract.PersonalRecordsEntry.COLUMN_SQUATCLEAN, 0);
            contentValues.put(PersonalRecordsContract.PersonalRecordsEntry.COLUMN_POWERCLEAN, 0);
            contentValues.put(PersonalRecordsContract.PersonalRecordsEntry.COLUMN_COMPLEXCLEAN, 0);
            contentValues.put(PersonalRecordsContract.PersonalRecordsEntry.COLUMN_CLEANANDJERK, 0);
        }

        long newPrsId = db.insert(PersonalRecordsContract.PersonalRecordsEntry.TABLE_NAME, null, contentValues);

        db.close();
    }

    private int mapValueOrEmpty(Map<String, Integer> map, LPO exercicio) {
        Integer value = map.get(exercicio.getNome());
        return value == null ? 0 : value;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_personal_information);
        setUpToolBar();
    }

    private void setUpToolBar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_personal_information);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
    }

    private void setUpPersonalContent(Pessoa pessoa) {
        // mProfilePicture = (ImageView) findViewById(R.id.iv_personal_information_profile_pic);
        /**
        Glide.with(this).load(urlProfileImg)
                .crossFade()
                .thumbnail(0.5f)
                .bitmapTransform(new CircleTransform(this))
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(mProfilePicture);
         **/

        mNameAndAgeTextView = (TextView) findViewById(R.id.tv_personal_information_name_and_age);
        mNameAndAgeTextView.setText(pessoa.getNome() + ", " + pessoa.getIdade());

        mBoxTextView = (TextView) findViewById(R.id.tv_personal_information_box);
        mBoxTextView.setText(pessoa.getBox());

        mPointsTextView = (TextView) findViewById(R.id.tv_personal_information_points);
        mPointsTextView.setText(String.valueOf(pessoa.getPontos()));

        mPhoneTextView = (TextView) findViewById(R.id.tv_personal_information_phone);
        mPhoneTextView.setText(pessoa.getCelular());

        mBirthdayTextView = (TextView) findViewById(R.id.tv_personal_information_date);
        mBirthdayTextView.setText(pessoa.getAniversario());

        mChangeUsernameTextView = (TextView) findViewById(R.id.tv_personal_information_change_username);
        mChangeUsernameTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        mChangePasswordTextView = (TextView) findViewById(R.id.tv_personal_information_change_password);
        mChangePasswordTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
