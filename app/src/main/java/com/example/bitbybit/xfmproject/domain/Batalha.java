package com.example.bitbybit.xfmproject.domain;

import java.util.List;

/**
 * Created by filipelobo on 27/06/2017.
 */

public class Batalha {

    private List<Exercicio> exercicios;
    private int minutosConclusao;

    public List<Exercicio> getExercicios() {
        return exercicios;
    }

    public void setExercicios(List<Exercicio> exercicios) {
        this.exercicios = exercicios;
    }

    public int getMinutosConclusao() {
        return minutosConclusao;
    }

    public void setMinutosConclusao(int minutosConclusao) {
        this.minutosConclusao = minutosConclusao;
    }


}