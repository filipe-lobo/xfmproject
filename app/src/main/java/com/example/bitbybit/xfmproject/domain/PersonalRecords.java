package com.example.bitbybit.xfmproject.domain;

import com.example.bitbybit.xfmproject.domain.enums.Exercicios;
import com.example.bitbybit.xfmproject.domain.enums.LPO;

import java.util.EnumMap;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by filipelobo on 26/05/2017.
 */

public class PersonalRecords extends GenericDomain {

    private Map<String,Integer> prs;

    public PersonalRecords() {
        this.prs = new HashMap<String,Integer>();
    }

    public Map<String,Integer> getConjuntoPRs() {
        return prs;
    }

    public void setConjuntoPRs(Map<String,Integer> prs) {
        this.prs = prs;
    }

    @Override
    public String toString() {
        return "PersonalRecords{id=" + id + ", prs=" + prs + '}';
    }

}
