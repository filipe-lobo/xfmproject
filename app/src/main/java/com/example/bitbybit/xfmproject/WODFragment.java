package com.example.bitbybit.xfmproject;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.example.bitbybit.xfmproject.controller.WODItemAdapter;
import com.example.bitbybit.xfmproject.dao.contract.TreinoContract;
import com.example.bitbybit.xfmproject.dao.contract.WebtokenContract;
import com.example.bitbybit.xfmproject.dao.helper.CrossFDbHelper;
import com.example.bitbybit.xfmproject.domain.Pessoa;
import com.example.bitbybit.xfmproject.domain.Treino;
import com.example.bitbybit.xfmproject.domain.enums.Box;

import org.w3c.dom.Text;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class WODFragment extends Fragment implements ActivityCompat.OnRequestPermissionsResultCallback {

    private static final String EXTRA_MESSAGE_BOX = "box";
    private static final int MY_PERMISSIONS_REQUEST_CAMERA = 10;
    private final Pessoa mPessoa;
    private View mRootView;
    /**
    private Button mStartButton, mResetButton;
    private Chronometer mChronometer;
    **/

    private ImageButton mQrCodeButton;
    private TextView mCameraWarningTextView;

    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;

    private CrossFDbHelper mCrossFDbHelper;

    private Treino mTreino;

    private boolean isStartButtonPressed = false;
    private long lastPaused = 0;

    public WODFragment(Pessoa pessoa) {
        this.mPessoa = pessoa;
        // Required empty public constructor
    }

    public static WODFragment newInstance(Pessoa pessoa) {
        WODFragment fragment = new WODFragment(pessoa);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    private Treino getTreinoFromDb() {
        ArrayList<Treino> array = new ArrayList<Treino>();
        String selectQuery = "SELECT * FROM " + TreinoContract.TreinoEntry.TABLE_NAME;
        SQLiteDatabase db = mCrossFDbHelper.getReadableDatabase();
        try {
            Cursor cursor = db.rawQuery(selectQuery, null);
            try {
                if(cursor.moveToFirst()) {
                    do {
                        Treino treino = new Treino();
                        treino.setBox(cursor.getString(1));
                        treino.getUsuario().setUsuario(cursor.getString(2));
                        treino.setData(cursor.getString(3));
                        treino.setWodPrincipal(cursor.getString(4));
                        treino.setWodDesafio(cursor.getString(5));
                        treino.setWodExtra(cursor.getString(6));
                        treino.setTipo(cursor.getInt(7) != 0 ? true : false);
                        array.add(treino);
                    } while(cursor.moveToNext());
                }
            } finally {
                try {
                    cursor.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } finally {
            try {
                db.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return array.get(0);
    }

    private Cursor checkIfDbHasWODEntries() {
        String selectionQuery = "SELECT * FROM " + TreinoContract.TreinoEntry.TABLE_NAME;
        SQLiteDatabase db = mCrossFDbHelper.getReadableDatabase();
        try {
            Cursor cursor = db.rawQuery(selectionQuery, null);
            if(cursor.getCount() > 0) {
                db.close();
                return cursor;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    private void reloadTreinoEntries() {
        mCrossFDbHelper = new CrossFDbHelper(getContext());
        if(checkIfDbHasWODEntries() != null) {
            mTreino = getTreinoFromDb();
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
            Date date = new Date();
            String dateString = simpleDateFormat.format(date);
            if(dateString.equals(mTreino.getData())) {
                if(mRecyclerView != null) {
                    mLayoutManager = new LinearLayoutManager(getContext());
                    mRecyclerView.setLayoutManager(mLayoutManager);
                    mAdapter = new WODItemAdapter(getContext(), mTreino);
                    mRecyclerView.setAdapter(mAdapter);
                    mQrCodeButton.setVisibility(View.GONE);
                    mCameraWarningTextView.setVisibility(View.GONE);
                    mRecyclerView.setVisibility(View.VISIBLE);
                }
            } else {
                SQLiteDatabase db = mCrossFDbHelper.getWritableDatabase();
                db.delete(TreinoContract.TreinoEntry.TABLE_NAME, "1", null);
                db.close();
                mRecyclerView.setVisibility(View.GONE);
                mQrCodeButton.setVisibility(View.VISIBLE);
                mCameraWarningTextView.setVisibility(View.VISIBLE);
            }
        } else {
            mRecyclerView.setVisibility(View.GONE);
            mQrCodeButton.setVisibility(View.VISIBLE);
            mCameraWarningTextView.setVisibility(View.VISIBLE);
        }
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        setWodLayout(inflater, container, savedInstanceState);
        return mRootView;
    }

    private void setWodLayout(LayoutInflater inflater, ViewGroup container,
                              Bundle savedInstanceState) {
        mRootView = inflater.inflate(R.layout.fragment_wod, container, false);

        mRecyclerView = (RecyclerView) mRootView.findViewById(R.id.rv_wod_items);
        mQrCodeButton = (ImageButton) mRootView.findViewById(R.id.bttn_qr_code);
        mCameraWarningTextView = (TextView) mRootView.findViewById(R.id.tv_fragment_wod_camera_warning);

        reloadTreinoEntries();

        mQrCodeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ContextCompat.checkSelfPermission(getActivity(),
                        Manifest.permission.CAMERA)
                        != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(getActivity(),
                            new String[]{Manifest.permission.CAMERA},
                            MY_PERMISSIONS_REQUEST_CAMERA);
                } else {
                    if(mPessoa.getBox() != null) {
                        Intent intent = new Intent(getContext(), QRCodeActivity.class);
                        intent.putExtra(EXTRA_MESSAGE_BOX, mPessoa.getBox());
                        startActivity(intent);
                    }
                }
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        reloadTreinoEntries();
    }
}
