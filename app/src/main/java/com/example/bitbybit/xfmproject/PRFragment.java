package com.example.bitbybit.xfmproject;

import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import com.example.bitbybit.xfmproject.controller.PRItemAdapter;
import com.example.bitbybit.xfmproject.domain.PersonalRecords;
import com.example.bitbybit.xfmproject.domain.Pessoa;
import com.example.bitbybit.xfmproject.domain.enums.LPO;
import java.util.ArrayList;
import java.util.EnumSet;

public class PRFragment extends Fragment implements MainActivity.PessoaChangeListener {

    private PRItemAdapter mPRItemAdapter;
    private RecyclerView mRecyclerView;
    private EditText mSearchEditText;
    private Pessoa mPessoa;

    public PRFragment() {
        // Required empty public constructor
    }

    public static PRFragment newInstance(Pessoa pessoa) {
        PRFragment fragment = new PRFragment();
        fragment.mPessoa = pessoa;
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_pr, container, false);

        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.rv_exercise_items);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(rootView.getContext(), LinearLayoutManager.VERTICAL, false);
        mRecyclerView.setLayoutManager(linearLayoutManager);
        mRecyclerView.setHasFixedSize(true);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(mRecyclerView.getContext(),
                linearLayoutManager.getOrientation());
        mRecyclerView.addItemDecoration(dividerItemDecoration);

        setRecyclerViewData(mPessoa);

        mSearchEditText = (EditText) rootView.findViewById(R.id.et_progress_search);
        mSearchEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                filter(s.toString());
            }
        });

        return rootView;
    }

    public void setRecyclerViewData(Pessoa pessoa){
        try {
            mPRItemAdapter = new PRItemAdapter(getContext(), mPessoa.getPrs());
        } catch(Exception e) {
            mPRItemAdapter = new PRItemAdapter(getContext(), new PersonalRecords());
            e.printStackTrace();
        }
        mRecyclerView.setAdapter(mPRItemAdapter);
    }

    private void filter(String text){
        ArrayList<LPO> temp = new ArrayList<LPO>();
        ArrayList<LPO> displayedList = new ArrayList(EnumSet.allOf(LPO.class));
        for(LPO e: displayedList){
            if(e.toString().toLowerCase().contains(text.toLowerCase())){
                temp.add(e);
            }
        }
        mPRItemAdapter.updateList(temp);
    }

    @Override
    public void onChange(Pessoa pessoa) {
        mPessoa = pessoa;
        setRecyclerViewData(mPessoa);
    }
}
