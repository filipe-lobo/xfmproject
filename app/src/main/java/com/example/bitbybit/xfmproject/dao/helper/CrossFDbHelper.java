package com.example.bitbybit.xfmproject.dao.helper;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.example.bitbybit.xfmproject.dao.contract.PersonalRecordsContract;
import com.example.bitbybit.xfmproject.dao.contract.PessoaContract;
import com.example.bitbybit.xfmproject.dao.contract.TreinoContract;
import com.example.bitbybit.xfmproject.dao.contract.UsuarioContract;
import com.example.bitbybit.xfmproject.dao.contract.WebtokenContract.*;
import com.example.bitbybit.xfmproject.domain.Webtoken;

/**
 * Created by filipelobo on 04/06/2017.
 */

public class CrossFDbHelper extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "crossf.db";

    private static final int DATABASE_VERSION = 1;

    public CrossFDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        final String SQL_CREATE_WEBTOKEN_TABLE = "CREATE TABLE " + WebtokenEntry.TABLE_NAME + " (" +
                WebtokenEntry._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                WebtokenEntry.COLUMN_PRIMARY_KEY_PESSOA + " INTEGER NOT NULL, " +
                WebtokenEntry.COLUMN_USUARIO + " TEXT NOT NULL, " +
                WebtokenEntry.COLUMN_IS_PROFESSOR + " INTEGER NOT NULL" +
                "); ";

        final String SQL_CREATE_PESSOA_TABLE = "CREATE TABLE " + PessoaContract.PessoaEntry.TABLE_NAME + " (" +
                PessoaContract.PessoaEntry._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                PessoaContract.PessoaEntry.COLUMN_NOME + " TEXT NOT NULL, " +
                PessoaContract.PessoaEntry.COLUMN_IDADE + " INTEGER, " +
                PessoaContract.PessoaEntry.COLUMN_ANIVERSARIO + " TEXT, " +
                PessoaContract.PessoaEntry.COLUMN_CELULAR + " TEXT, " +
                PessoaContract.PessoaEntry.COLUMN_BOX + " TEXT NOT NULL, " +
                PessoaContract.PessoaEntry.COLUMN_VENCIMENTO + " TEXT, " +
                PessoaContract.PessoaEntry.COLUMN_PONTOS + " INTEGER NOT NULL" +
                "); ";

        final String SQL_CREATE_PRS_TABLE = "CREATE TABLE " + PersonalRecordsContract.PersonalRecordsEntry.TABLE_NAME + " (" +
                PersonalRecordsContract.PersonalRecordsEntry._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                PersonalRecordsContract.PersonalRecordsEntry.COLUMN_SNATCH + " INTEGER, " +
                PersonalRecordsContract.PersonalRecordsEntry.COLUMN_SQUATSNATCH + " INTEGER, " +
                PersonalRecordsContract.PersonalRecordsEntry.COLUMN_POWERSNATCH + " INTEGER, " +
                PersonalRecordsContract.PersonalRecordsEntry.COLUMN_SNATCHBALANCE + " INTEGER, " +
                PersonalRecordsContract.PersonalRecordsEntry.COLUMN_COMPLEXSNATCH + " INTEGER, " +
                PersonalRecordsContract.PersonalRecordsEntry.COLUMN_CLEAN + " INTEGER, " +
                PersonalRecordsContract.PersonalRecordsEntry.COLUMN_SQUATCLEAN + " INTEGER, " +
                PersonalRecordsContract.PersonalRecordsEntry.COLUMN_POWERCLEAN + " INTEGER, " +
                PersonalRecordsContract.PersonalRecordsEntry.COLUMN_COMPLEXCLEAN + " INTEGER, " +
                PersonalRecordsContract.PersonalRecordsEntry.COLUMN_CLEANANDJERK + " INTEGER" +
                "); ";

        final String SQL_CREATE_USUARIO_TABLE = "CREATE TABLE " + UsuarioContract.UsuarioEntry.TABLE_NAME + " (" +
                UsuarioContract.UsuarioEntry._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                UsuarioContract.UsuarioEntry.COLUMN_USUARIO + " TEXT NOT NULL, " +
                UsuarioContract.UsuarioEntry.COLUMN_ID_PESSOA + " INTEGER NOT NULL, " +
                UsuarioContract.UsuarioEntry.COLUMN_IS_ATIVO + " INTEGER NOT NULL, " +
                UsuarioContract.UsuarioEntry.COLUMN_IS_PROFESSOR + " INTEGER NOT NULL" +
                "); ";

        final String SQL_CREATE_TREINO_TABLE = "CREATE TABLE " + TreinoContract.TreinoEntry.TABLE_NAME + " (" +
                TreinoContract.TreinoEntry._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                TreinoContract.TreinoEntry.COLUMN_BOX + " TEXT NOT NULL, " +
                TreinoContract.TreinoEntry.COLUMN_USUARIO + " TEXT, " +
                TreinoContract.TreinoEntry.COLUMN_DATA + " TEXT NOT NULL, " +
                TreinoContract.TreinoEntry.COLUMN_WOD_PRINCIPAL + " TEXT NOT NULL, " +
                TreinoContract.TreinoEntry.COLUMN_WOD_DESAFIO + " TEXT NOT NULL, " +
                TreinoContract.TreinoEntry.COLUMN_WOD_EXTRA + " TEXT, " +
                TreinoContract.TreinoEntry.COLUMN_TIPO + " INTEGER NOT NULL" +
                "); ";

        db.execSQL(SQL_CREATE_WEBTOKEN_TABLE);
        db.execSQL(SQL_CREATE_PESSOA_TABLE);
        db.execSQL(SQL_CREATE_PRS_TABLE);
        db.execSQL(SQL_CREATE_USUARIO_TABLE);
        db.execSQL(SQL_CREATE_TREINO_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        final String SQL_DELETE_WEBTOKEN_TABLE = "DROP TABLE IF EXISTS " + WebtokenEntry.TABLE_NAME;
        final String SQL_DELETE_PESSOA_TABLE = "DROP TABLE IF EXISTS " + PessoaContract.PessoaEntry.TABLE_NAME;
        final String SQL_DELETE_PRS_TABLE = "DROP TABLE IF EXISTS " + PersonalRecordsContract.PersonalRecordsEntry.TABLE_NAME;
        final String SQL_DELETE_USUARIO_TABLE = "DROP TABLE IF EXISTS " + UsuarioContract.UsuarioEntry.TABLE_NAME;
        final String SQL_DELETE_TREINO_TABLE = "DROP TABLE IF EXISTS " + TreinoContract.TreinoEntry.TABLE_NAME;

        db.execSQL(SQL_DELETE_WEBTOKEN_TABLE);
        db.execSQL(SQL_DELETE_PESSOA_TABLE);
        db.execSQL(SQL_DELETE_PRS_TABLE);
        db.execSQL(SQL_DELETE_USUARIO_TABLE);
        db.execSQL(SQL_DELETE_TREINO_TABLE);

        onCreate(db);
    }

    @Override
    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onUpgrade(db, oldVersion, newVersion);
    }

}
