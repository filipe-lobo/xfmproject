package com.example.bitbybit.xfmproject;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.example.bitbybit.xfmproject.dao.contract.PersonalRecordsContract;
import com.example.bitbybit.xfmproject.dao.helper.CrossFDbHelper;
import com.example.bitbybit.xfmproject.domain.PersonalRecords;
import com.example.bitbybit.xfmproject.domain.enums.LPO;
import com.example.bitbybit.xfmproject.webservice.XFManagerWSClient;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.loopj.android.http.RequestParams;
import com.loopj.android.http.TextHttpResponseHandler;

import cz.msebera.android.httpclient.Header;

public class UpdatePRDialogFragment extends DialogFragment {

    private LPO mExercicio;
    private PersonalRecords mPrs;

    public UpdatePRDialogFragment(LPO exercicio, PersonalRecords prs) {
        this.mExercicio = exercicio;
        this.mPrs = prs;
    }

    public interface UpdatePRDialogListener {
        public void onDialogPositiveClick(DialogFragment dialog);
    }

    UpdatePRDialogListener mListener;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            mListener = (UpdatePRDialogListener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString()
                    + " must implement NoticeDialogListener");
        }
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();

        View rootView = inflater.inflate(R.layout.dialog_update_pr, null);
        TextView exerciseLabel = (TextView) rootView.findViewById(R.id.tv_dialog_exercise_label);
        final EditText weightEditText = (EditText) rootView.findViewById(R.id.et_dialog_exercise_new_weight);
        exerciseLabel.setText(mExercicio.toString());

        builder.setView(rootView)
                .setPositiveButton(R.string.confirm, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        Log.d("POSITIVE", "User touched the dialog's positive button");
                        String text = weightEditText.getText().toString();
                        if(!text.equals("")) {
                            final int newPr = Integer.valueOf(text);

                            mPrs.getConjuntoPRs().put(mExercicio.getNome(), newPr);
                            String url = "usuario/cadastrarpersonalrecords";
                            RequestParams params = new RequestParams();
                            String prs = new Gson().toJson(mPrs);
                            params.put("prs", prs);

                            XFManagerWSClient.post(url, params, new TextHttpResponseHandler() {
                                @Override
                                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                                    Log.e("FAILURE", responseString);
                                }

                                @Override
                                public void onSuccess(int statusCode, Header[] headers, String responseString) {
                                    Log.d("SUCCESS", responseString);
                                }
                            });
                            updatePRSTable(newPr);
                            mListener.onDialogPositiveClick(UpdatePRDialogFragment.this);
                        }
                    }
                })
                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        Log.d("NEGATIVE", "User touched the dialog's negative button");
                    }
                });
        return builder.create();

    }

    private void updatePRSTable(int value) {
        CrossFDbHelper crossFDbHelper = new CrossFDbHelper(getActivity());
        SQLiteDatabase db = crossFDbHelper.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(returnColumnAccordingToExercise(mExercicio.getNome()), value);

        int count = db.update(
                PersonalRecordsContract.PersonalRecordsEntry.TABLE_NAME,
                values,
                null,
                null);
    }

    private String returnColumnAccordingToExercise(String exercicio) {
        switch(exercicio) {
            case "Snatch":
                return PersonalRecordsContract.PersonalRecordsEntry.COLUMN_SNATCH;
            case "Squat Snatch":
                return PersonalRecordsContract.PersonalRecordsEntry.COLUMN_SQUATSNATCH;
            case "Power Snatch":
                return PersonalRecordsContract.PersonalRecordsEntry.COLUMN_POWERSNATCH;
            case "Snatch Balance":
                return PersonalRecordsContract.PersonalRecordsEntry.COLUMN_SNATCHBALANCE;
            case "Complex Snatch":
                return PersonalRecordsContract.PersonalRecordsEntry.COLUMN_COMPLEXSNATCH;
            case "Clean":
                return PersonalRecordsContract.PersonalRecordsEntry.COLUMN_CLEAN;
            case "Squat Clean":
                return PersonalRecordsContract.PersonalRecordsEntry.COLUMN_SQUATCLEAN;
            case "Power Clean":
                return PersonalRecordsContract.PersonalRecordsEntry.COLUMN_POWERCLEAN;
            case "Complex Clean":
                return PersonalRecordsContract.PersonalRecordsEntry.COLUMN_COMPLEXCLEAN;
            case "Clean and Jeark":
                return PersonalRecordsContract.PersonalRecordsEntry.COLUMN_CLEANANDJERK;
            default:
                return "";
        }
    }
}
