package com.example.bitbybit.xfmproject.domain;

/**
 * Created by filipelobo on 22/05/2017.
 */

public class Webtoken {
    private int primarykey_pessoa;
    private String usuario;
    private boolean isProfessor;

    public Webtoken() {

    }

    public int getPrimarykey_pessoa() {
        return primarykey_pessoa;
    }

    public void setPrimarykey_pessoa(int primarykey_pessoa) {
        this.primarykey_pessoa = primarykey_pessoa;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public boolean isProfessor() {
        return isProfessor;
    }

    public void setProfessor(boolean professor) {
        isProfessor = professor;
    }
}