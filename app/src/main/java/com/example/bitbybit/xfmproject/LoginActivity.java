package com.example.bitbybit.xfmproject;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.app.DatePickerDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.icu.util.Calendar;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.DatePicker;
import android.support.design.widget.TextInputEditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import com.example.bitbybit.xfmproject.dao.contract.UsuarioContract;
import com.example.bitbybit.xfmproject.dao.helper.CrossFDbHelper;
import com.example.bitbybit.xfmproject.domain.PersonalRecords;
import com.example.bitbybit.xfmproject.domain.Pessoa;
import com.example.bitbybit.xfmproject.domain.Usuario;
import com.example.bitbybit.xfmproject.domain.enums.Box;
import com.example.bitbybit.xfmproject.utils.JWTUtils;
import com.example.bitbybit.xfmproject.webservice.XFManagerWSClient;
import com.google.gson.Gson;
import com.loopj.android.http.RequestParams;
import com.loopj.android.http.TextHttpResponseHandler;
import org.joda.time.LocalDate;
import org.joda.time.Years;
import java.text.SimpleDateFormat;
import java.util.Date;
import cz.msebera.android.httpclient.Header;

public class LoginActivity extends AppCompatActivity {

    private AutoCompleteTextView mEmailView;
    private TextInputEditText mPasswordView;

    private TextInputEditText mNewUsernameView, mNewPasswordView, mNewRepeatPasswordView, mNewUsernameNameView;
    Button mNewDatePickerButton;
    private TextInputEditText mNewPhoneNumberView;
    private Spinner mNewBoxPickerView;
    private Button mNewUserButton;
    private DatePickerDialog.OnDateSetListener datePickerListener = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
            mNewDatePickerButton.setText(String.format(String.format("%04d", year) + "-" + String.format("%02d", (month+1)) + "-" + String.format("%02d", dayOfMonth)));
        }
    };

    private TextView mGoToNewUserView, mGoToLoginView;

    private View mProgressView;
    private View mLoginFormView, mNewUserFormView;
    private Context mContext = this;

    private CrossFDbHelper mCrossFDbHelper;

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        mEmailView = (AutoCompleteTextView) findViewById(R.id.email);

        mPasswordView = (TextInputEditText) findViewById(R.id.password);
        mPasswordView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if (id == R.id.login || id == EditorInfo.IME_NULL) {
                    attemptLogin();
                    return true;
                }
                return false;
            }
        });

        Button mEmailSignInButton = (Button) findViewById(R.id.email_sign_in_button);
        mEmailSignInButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                attemptLogin();
            }
        });

        mLoginFormView = findViewById(R.id.login_form);
        mProgressView = findViewById(R.id.login_progress);
        mGoToNewUserView = (TextView) findViewById(R.id.tv_login_go_to_new_user);
        mGoToNewUserView.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                mLoginFormView.setVisibility(View.GONE);
                mNewUserFormView.setVisibility(View.VISIBLE);
            }
        });

        mCrossFDbHelper = new CrossFDbHelper(this);

        loadCreateNewUserView();

    }

    private void loadCreateNewUserView() {
        mNewUserFormView = findViewById(R.id.new_user_form);

        mNewUsernameView = (TextInputEditText) findViewById(R.id.new_user_username);
        mNewPasswordView = (TextInputEditText) findViewById(R.id.new_user_password);
        mNewRepeatPasswordView = (TextInputEditText) findViewById(R.id.new_user_repeat_password);
        mNewUsernameNameView = (TextInputEditText) findViewById(R.id.new_user_name);
        mNewDatePickerButton = (Button) findViewById(R.id.new_user_pick_date_button);

        mNewPhoneNumberView = (TextInputEditText) findViewById(R.id.new_user_phone_number);
        mNewPhoneNumberView.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                mNewPhoneNumberView.removeTextChangedListener(this);
                String oldPhoneNumber = s.toString().replaceAll("\\D+","");
                String newPhoneNumber = "";
                if(oldPhoneNumber.length() > 2) {
                    newPhoneNumber = "(" + oldPhoneNumber.substring(0,2) + ")";
                    if(oldPhoneNumber.length() > 10) {
                        newPhoneNumber = newPhoneNumber + oldPhoneNumber.substring(2,7) + "-" + oldPhoneNumber.substring(7,11);
                    } else if(oldPhoneNumber.length() > 6) {
                        newPhoneNumber = newPhoneNumber + oldPhoneNumber.substring(2,6) + "-" + oldPhoneNumber.substring(6);
                    } else {
                        newPhoneNumber = newPhoneNumber + oldPhoneNumber.substring(2);
                    }
                } else {
                    newPhoneNumber = oldPhoneNumber;
                }
                mNewPhoneNumberView.setText(newPhoneNumber);
                mNewPhoneNumberView.setSelection(newPhoneNumber.length());
                mNewPhoneNumberView.addTextChangedListener(this);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        mNewBoxPickerView = (Spinner) findViewById(R.id.new_user_pick_box);

        mNewUserButton = (Button) findViewById(R.id.new_user_button);
        mNewUserButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                attemptSavingNewUser();
            }
        });

        ArrayAdapter<Box> adapter = new ArrayAdapter<Box>(this,
                android.R.layout.simple_spinner_item, Box.values());
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mNewBoxPickerView.setAdapter(adapter);

        mGoToLoginView = (TextView) findViewById(R.id.tv_new_user_go_to_login);
        mGoToLoginView.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                mNewUserFormView.setVisibility(View.GONE);
                mLoginFormView.setVisibility(View.VISIBLE);
            }
        });
    }

    private void attemptSavingNewUser() {

        mNewUsernameView.setError(null);
        mNewPasswordView.setError(null);
        mNewRepeatPasswordView.setError(null);
        mNewUsernameNameView.setError(null);
        mNewPhoneNumberView.setError(null);
        mNewDatePickerButton.setError(null);

        String username = mNewUsernameView.getText().toString();
        String password = mNewPasswordView.getText().toString();
        String repeatPassword = mNewRepeatPasswordView.getText().toString();
        String name = mNewUsernameNameView.getText().toString();
        String phone = mNewPhoneNumberView.getText().toString();
        String date = mNewDatePickerButton.getText().toString();

        boolean cancel = false;
        View focusView = null;

        if (TextUtils.isEmpty(username)) {
            mNewUsernameView.setError(getString(R.string.error_invalid_username));
            focusView = mNewUsernameView;
            cancel = true;
        }

        if (!TextUtils.isEmpty(username) && !isTextFieldValid(username)) {
            mNewUsernameView.setError(getString(R.string.error_invalid_username));
            focusView = mNewUsernameView;
            cancel = true;
        }

        if (TextUtils.isEmpty(password)) {
            mNewPasswordView.setError(getString(R.string.error_field_required));
            focusView = mNewPasswordView;
            cancel = true;
        }

        if (!TextUtils.isEmpty(password) && !isTextFieldValid(password)) {
            mNewPasswordView.setError(getString(R.string.error_invalid_password));
            focusView = mNewPasswordView;
            cancel = true;
        }

        if(TextUtils.isEmpty(repeatPassword)) {
            mNewRepeatPasswordView.setError(getString(R.string.error_password_unconfirmed));
            focusView = mNewRepeatPasswordView;
            cancel = true;
        }

        if(!TextUtils.isEmpty(repeatPassword) && !password.equals(repeatPassword)) {
            mNewRepeatPasswordView.setError(getString(R.string.error_different_passwords));
            focusView = mNewRepeatPasswordView;
            cancel = true;
        }

        if (date.equals(getString(R.string.new_user_pick_date))) {
            mNewDatePickerButton.setError(getString(R.string.error_invalid_date));
            focusView = mNewDatePickerButton;
            cancel = true;
        }

        if(!isPhoneNumberCorrect(phone)) {
            mNewPhoneNumberView.setError(getString(R.string.error_invalid_phone));
            focusView = mNewPhoneNumberView;
            cancel = true;
        }

        if(TextUtils.isEmpty(name)) {
            mNewUsernameNameView.setError(getString(R.string.error_field_required));
            focusView = mNewUsernameNameView;
            cancel = true;
        }

        if (cancel) {
            focusView.requestFocus();
        } else {

            showProgressInsteadOfNewUser(true);
            requestNewWebToken();
        }

    }

    private boolean isPhoneNumberCorrect(String phone) {
        String onlyDigits = phone.replaceAll("\\D+","");
        if(onlyDigits.length() < 10) {
            return false;
        } else {
            return true;
        }
    }

    private void requestNewWebToken() {

        Usuario usuario = new Usuario(mNewUsernameView.getText().toString(), mNewPasswordView.getText().toString(), false);

        Pessoa pessoa = new Pessoa();
        pessoa.setUsuario(usuario);
        pessoa.setNome(mNewUsernameNameView.getText().toString());

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        try {
            Date birthday = dateFormat.parse(mNewDatePickerButton.getText().toString());
            pessoa.setAniversario(mNewDatePickerButton.getText().toString());
            int age = Years.yearsBetween(LocalDate.fromDateFields(birthday), LocalDate.now()).getYears();
            pessoa.setIdade(age);
            System.out.println("Age: " + age);
        } catch(Exception e) {
            e.printStackTrace();
        }

        pessoa.setCelular(mNewPhoneNumberView.getText().toString());
        pessoa.setVencimento(null);
        pessoa.setPontos(0);

        PersonalRecords prs = new PersonalRecords();
        pessoa.setPrs(prs);
        pessoa.setBox(mNewBoxPickerView.getSelectedItem().toString());

        Gson gson = new Gson();
        String pessoaJson = gson.toJson(pessoa);
        Log.d("TAG", pessoaJson);

        RequestParams params = new RequestParams();
        params.put("pessoa", pessoaJson);
        String jwtString = "usuario/novousuario";

        XFManagerWSClient.post(jwtString, params, new TextHttpResponseHandler() {

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                Log.d("FAILURE", responseString);
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        showProgressInsteadOfNewUser(false);
                    }
                });
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseString) {
                Log.d("SUCCESS", responseString);
                if (responseString.isEmpty()) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            showProgressInsteadOfNewUser(false);
                        }
                    });
                } else {
                    try {
                        String jwt = JWTUtils.decoded(responseString);
                        if (!(jwt.isEmpty())) {
                            Gson gson = new Gson();
                            Usuario usuario = gson.fromJson(jwt, Usuario.class);
                            saveWebtokenOnDb(usuario);

                            Toast toast = Toast.makeText(mContext, getString(R.string.toast_new_user_success), Toast.LENGTH_SHORT);
                            toast.show();

                            Intent intent = new Intent(getBaseContext(), MainActivity.class);
                            startActivity(intent);
                            finish();
                        }
                    } catch (Exception e) {
                        showProgressInsteadOfNewUser(false);
                        e.printStackTrace();
                    }
                }
            }
        });
    }

    private void attemptLogin() {

        mEmailView.setError(null);
        mPasswordView.setError(null);

        String email = mEmailView.getText().toString();
        String password = mPasswordView.getText().toString();

        boolean cancel = false;
        View focusView = null;

        if (TextUtils.isEmpty(email)) {
            mEmailView.setError(getString(R.string.error_field_required));
            focusView = mEmailView;
            cancel = true;
        }

        if (TextUtils.isEmpty(password)) {
            mPasswordView.setError(getString(R.string.error_field_required));
            focusView = mPasswordView;
            cancel = true;
        }

        if (!TextUtils.isEmpty(password) && !isTextFieldValid(password)) {
            mPasswordView.setError(getString(R.string.error_invalid_password));
            focusView = mPasswordView;
            cancel = true;
        }

        if (cancel) {
            focusView.requestFocus();
        } else {
            showProgressInsteadOfLogin(true);
            requestWebToken();
        }
    }

    private void requestWebToken() {

        String jwtString = String.format("usuario/" + mEmailView.getText().toString()
                + "/" + mPasswordView.getText().toString());

        XFManagerWSClient.get(jwtString, null, new TextHttpResponseHandler() {

            @Override
            public void onFailure(int statusCode, cz.msebera.android.httpclient.Header[] headers, String responseString, Throwable throwable) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        showProgressInsteadOfLogin(false);
                    }
                });
            }

            @Override
            public void onSuccess(int statusCode, cz.msebera.android.httpclient.Header[] headers, String responseString) {
                if (responseString.isEmpty()) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            showProgressInsteadOfLogin(false);
                        }
                    });
                } else {
                    try {
                        String jwt = JWTUtils.decoded(responseString);
                        Log.d("WEBTOKEN", jwt);
                        if (!(jwt.isEmpty())) {
                            Gson gson = new Gson();
                            Usuario usuario = gson.fromJson(jwt, Usuario.class);
                            saveWebtokenOnDb(usuario);

                            Intent intent = new Intent(getBaseContext(), MainActivity.class);
                            startActivity(intent);
                            finish();
                        }
                    } catch (Exception e) {
                        showProgressInsteadOfLogin(false);
                        e.printStackTrace();
                    }
                }
            }
        });
    }

    private long saveWebtokenOnDb(Usuario usuario) {
        SQLiteDatabase db = mCrossFDbHelper.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(UsuarioContract.UsuarioEntry.COLUMN_ID_PESSOA, usuario.getIdPessoa());
        values.put(UsuarioContract.UsuarioEntry.COLUMN_IS_ATIVO, usuario.isAtivo());
        values.put(UsuarioContract.UsuarioEntry.COLUMN_IS_PROFESSOR, usuario.isProfessor());
        values.put(UsuarioContract.UsuarioEntry.COLUMN_USUARIO, usuario.getUsuario());

        long newRowId = db.insert(UsuarioContract.UsuarioEntry.TABLE_NAME, null, values);
        Log.d("ID", String.valueOf(newRowId));
        db.close();
        return newRowId;
    }

    private boolean isTextFieldValid(String text) {
        return text.length() > 4;
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgressInsteadOfLogin(final boolean show) {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
            mLoginFormView.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgressInsteadOfNewUser(final boolean show) {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            mNewUserFormView.setVisibility(show ? View.GONE : View.VISIBLE);
            mNewUserFormView.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mNewUserFormView.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mNewUserFormView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    public void showDatePickerDialog(View view) {
        final Calendar c = Calendar.getInstance();
        int mYear = c.get(Calendar.YEAR);
        int mMonth = c.get(Calendar.MONTH);
        int mDay = c.get(Calendar.DAY_OF_MONTH);
        DatePickerDialog dateDialog= new DatePickerDialog(this, datePickerListener, mYear, mMonth, mDay);
        dateDialog.show();
    }

    @Override
    protected void onDestroy() {
        mCrossFDbHelper.close();
        super.onDestroy();
    }
}

