package com.example.bitbybit.xfmproject.dao.contract;

import android.provider.BaseColumns;

/**
 * Created by filipelobo on 09/06/2017.
 */

public class PersonalRecordsContract {
    public static final class PersonalRecordsEntry implements BaseColumns {
        public static final String TABLE_NAME = "personalrecords";
        public static final String COLUMN_SNATCH = "snatch";
        public static final String COLUMN_SQUATSNATCH = "squatsnatch";
        public static final String COLUMN_POWERSNATCH = "powersnatch";
        public static final String COLUMN_SNATCHBALANCE = "snatchbalance";
        public static final String COLUMN_COMPLEXSNATCH = "complexsnatch";
        public static final String COLUMN_CLEAN = "clean";
        public static final String COLUMN_SQUATCLEAN = "squatclean";
        public static final String COLUMN_POWERCLEAN = "powerclean";
        public static final String COLUMN_COMPLEXCLEAN = "complexclean";
        public static final String COLUMN_CLEANANDJERK = "cleanandjerk";
    }
}
