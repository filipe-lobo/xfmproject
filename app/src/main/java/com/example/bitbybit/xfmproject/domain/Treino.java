package com.example.bitbybit.xfmproject.domain;

import com.example.bitbybit.xfmproject.domain.enums.*;
import com.example.bitbybit.xfmproject.domain.enums.Box;

/**
 * Created by filipelobo on 26/05/2017.
 */

public class Treino {

    private Box box;
    private Usuario usuario;
    private String data;
    private String wodPrincipal;
    private String wodDesafio;
    private String wodExtra;
    private boolean tipo;

    public Treino() {
        this.usuario = new Usuario();
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public Box getBox() {
        return box;
    }

    public void setBox(Box box) {
        this.box = box;
    }

    public void setBox(String box) {
        if (box.equals("Superação")) {
            this.box = Box.Superacao;
        }
    }

    public String getWodPrincipal() {
        return wodPrincipal;
    }

    public void setWodPrincipal(String wodPrincipal) {
        this.wodPrincipal = wodPrincipal;
    }

    public String getWodDesafio() {
        return wodDesafio;
    }

    public void setWodDesafio(String wodDesafio) {
        this.wodDesafio = wodDesafio;
    }

    public String getWodExtra() {
        return wodExtra;
    }

    public void setWodExtra(String wodExtra) {
        this.wodExtra = wodExtra;
    }

    public boolean isTipo() {
        return tipo;
    }

    public void setTipo(boolean tipo) {
        this.tipo = tipo;
    }

    @Override
    public String toString() {
        return "Treino{" + "box=" + box + ", usuario=" + usuario + ", data=" + data + ", wodPrincipal=" + wodPrincipal + ", wodDesafio=" + wodDesafio + ", wodExtra=" + wodExtra + ", tipo=" + tipo + '}';
    }

}
