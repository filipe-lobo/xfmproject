package com.example.bitbybit.xfmproject.domain;

/**
 * Created by filipelobo on 26/05/2017.
 */

public class Pessoa extends GenericDomain {

    private String nome;
    private int idade;
    private String aniversario;
    private String celular;
    private String box;
    private String vencimento;
    private PersonalRecords prs;
    private int pontos;
    private Usuario usuario;

    public Pessoa() {
        this.prs = new PersonalRecords();
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public int getIdade() {
        return idade;
    }

    public void setIdade(int idade) {
        this.idade = idade;
    }

    public String getAniversario() {
        return aniversario;
    }

    public void setAniversario(String aniversario) {
        this.aniversario = aniversario;
    }

    public String getCelular() {
        return celular;
    }

    public void setCelular(String celular) {
        this.celular = celular;
    }

    public String getBox() {
        return box;
    }

    public void setBox(Box box) {
        this.box = box.getNome();
    }

    public void setBox(String box) {
        this.box = box;
    }

    public PersonalRecords getPrs() {
        return this.prs;
    }

    public void setPrs(PersonalRecords prs) {
        this.prs = prs;
    }

    public int getPontos() {
        return pontos;
    }

    public void setPontos(int pontos) {
        this.pontos = pontos;
    }

    public String getVencimento() {
        return vencimento;
    }

    public void setVencimento(String vencimento) {
        this.vencimento = vencimento;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    @Override
    public String toString() {
        return "Pessoa{id=" + id + ", nome=" + nome + ", idade=" + idade + ", aniversario=" + aniversario + ", celular=" + celular + ", box=" + box + ", vencimento=" + vencimento + ", prs=" + prs + ", pontos=" + pontos + ", usuario=" + usuario + '}';
    }
}